# Precise motor mapping with transcranial magnetic stimulation
Konstantin Weise*, Ole Numssen*, Benjamin Kalloch, Anna Leah Zier, Axel Thielscher, Jens Haueisen, Gesa Hartwigsen+, Thomas R. Knösche+

This repository accompanies our protocol paper *Precise motor mapping with transcranial magnetic stimulation* (doi: [10.1038/s41596-022-00776-6](https://doi.org/10.1038/s41596-022-00776-6)). All scripts used in the protocol are managed here. An example data set is stored on [osf.io](https://doi.org/10.17605/OSF.IO/MYRQN).

![](misc/graphical_abstract.png)

We present a method to determine the cortical origin of motor evoked potentials (MEPs) with transcranial magnetic stimulation (TMS). In short, we perform stimulations with (many) different TMS coil positions and orientations around the primary motor cortex. For each pulse, we record the MEP from a finger muscle. After the experiment the electrical fields (E) for all stimulations are computed (-> [SimNIBS](http://simnibs.org)) and regressed on the MEPs. Finally, the cortical location of the best fit betwen E and MEP is determined by a goodness-of-fit measure.
This approach allows for a functional localization of the neuronal populations that drive the MEPs based on neuronal activation curves.

See the [changelog](CHANGELOG.md) for any updates on this scripts that might not have been included yet in the paper.

## [SimNIBS 4.x](https://github.com/simnibs/simnibs) support
We've adapted the scripts to support SimNIBS 4.x meshes (charm) and FEMs. The example data set now also holds an example charm mesh: `mesh0_refinedM1_charm`. Most steps in the protocol stay the same, except for the mesh refinement, which is now done directly with script #02: `02_make_msh_from_mri_simnibs4.py`. We will update the protocol to reflect this in the future.  
FEM computations are way faster with SimNIBS 4 and the mesh quality is significantly improved in comparison with SimNIBS 3.x. 

##


# Overview
## /scripts
All scripts to perform the cortical localization. Each script is extensively documented and shows some help about input parameters: `python scriptname.py -h`. These scripts are numbered to ease following along the workflow:

### Pre-experiment
- `create_fsaverage_roi.m`: Create a group-level region of interest .mgh file based on fsaverage BA parcellations.
- `01_create_subject_structure.py`: Initialize a subject-object template file and subject-object .hdf5 file.
- `02_make_msh_from_mri.py`: Use SimNIBS to generate the headmesh, based on subject-object parameters
    - For SimNIBS 4.x:`02_make_msh_from_mri_simnibs4.py`
- `03_refine_mesh.py`: Refine an existing mesh at a given region of interest (again, as defined in the subject-object file)
- `04_get_initial_m1_coords.py`: Tran: Use SimNIBS to generate the headmesh, based on subject-object parameterssform MNI coordinates to subject space to be used as cortical targets in the TMS experiment

### Post-experiment
- `05_merge_exp_data.py`: Preprocess and combine MEP and TMS data
- `06_calc_e.py`: Calculate all electric fields for the performed TMS experiment
- `07_calc_r2.py`: Calculate the cortical goodness-of-fits for the induced electric fields and the measured MEPs.
- `08_calc_opt_coil_pos.py`: Determine the coil position & orientation that optimally stimulates the cortical MEP origin.
- `09_estimate_rmt_from_data.py`: Estimate the resting motor threshold based on the results from `07_calc_r2.py`

## **/misc**
- Additional software: [IMporter](https://gitlab.gwdg.de/tms-localization/utils/importer) tool to import InstrumentMarkers for Localite TMSNavigator systems
- /paraview_states: [Paraview](https://www.paraview.org/) state files to visualize results and replicate protocol figures.

## **/examples**
- `roi_examples.md`: Examples for different ways to generate regions of interest (from MNI, from fsaverage, spherical, etc.)

# System requirements
## Hardware
- Standard workstation [min. 4 CPU cores, 16GB RAM, 500GB disc space per subject
- Operating System: 
   - Linux: 64 bit, tested on Ubuntu ≥ 16.04
   - MacOS ≥ 10.14 (M1 architecture: via rosetta framework)
   - Windows ≥ 7

## Software
- SimNIBS 4.x: [Python == 3.9](https://www.python.org) | SimNIBS 3.2.6: [Python == 3.7](https://www.python.org) managed with [(mini-)conda](http://conda.io)
- [SimNIBS 4.1.0](https://github.com/simnibs/simnibs/releases/tag/v4.1.0) | [SimNIBS 3.2.6](https://github.com/simnibs/simnibs/releases/tag/v3.2.6) for head model construction and electric field simulations
- [pyNIBS](https://pypi.org/project/pynibs/) python package
- optional: git ≥ 2.24.0 to clone this repository
- optional: [IMporter](https://gitlab.gwdg.de/tms-localization/importer) tool to import InstrumentMarker files into Localite TMS Navigator
- optional: [FSL 5.0.5](https://fsl.fmrib.ox.ac.uk) to construct diffusion tensors for anisotropic field calculations
- optional: [FreeSurfer >= 5.3.0](https://surfer.nmr.mgh.harvard.edu) to define regions of interest from the fsaverage template

# Installation
To run all scripts a Python environment is needed with SimNIBS and pyNIBS installed.  

## Linux
- Install a python environment handler (https://docs.conda.io/en/latest/miniconda.html)
- Install SimNIBS and pyNIBS
   ``` sh
   conda env update -f https://github.com/simnibs/simnibs/releases/download/v4.1.0/environment_linux.yml --name tms_loco
   conda activate tms_loco
   pip install https://github.com/simnibs/simnibs/releases/download/v4.1.0/simnibs-4.1.0-cp39-cp39-linux_x86_64.whl
   pip install pynibs
   ```
   Note: You can also add SimNIBS and pyNIBS to a pre-existing environment.
  To install SimNIBS 3.2.6 instead of SimNIBS 4choose the correct .yml and .whl files from the [release page](https://github.com/simnibs/simnibs/releases) and python 3.7.

- Clone this repository
   ``` sh
   cd folder/where/to/download/this/repository
   git clone https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto
   ```
Altenatively, download the newest release of this script repository: https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto/-/releases


## MacOS
- Install a python environment handler ([miniconda](https://docs.conda.io/en/latest/miniconda.html))
- For M1 architecture: make sure to install the M1 version of miniconda.
- Create `tms_loco` environment, set architecture to Rosetta:  
   ``` sh
   CONDA_SUBDIR=osx-64 conda create --name tms_loco python=3.9 
   conda activate tms_loco
   conda env config vars set CONDA_SUBDIR=osx-64
   conda activate base
   conda activate tms_loco
   ```
   You can also add simnibs and pyNIBS to a pre-existing environment, as long as the `osx-64` builds are used on M1.


- Install SimNIBS and pyNIBS
   ``` sh
   conda env update -f https://github.com/simnibs/simnibs/releases/download/v4.1.0/environment_macOS.yml --name tms_loco
   pip install https://github.com/simnibs/simnibs/releases/download/v4.1.0/simnibs-4.1.0-cp39-cp39-macosx_10_11_x86_64.whl
   pip install pynibs
   ```

- Clone this repository
   ``` sh
   cd folder/where/to/download/this/repository
   git clone https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto
   ```


## Windows
- Install a python environment handler (https://docs.conda.io/en/latest/miniconda.html)
- Recommend: Install git (https://git-for-windows.github.io/) to be able to keep the scripts we provide updated.
- Everything below is executed in the anaconda shell
- Install SimNIBS and pyNIBS
   ``` sh
   conda env update -f https://github.com/simnibs/simnibs/releases/download/v4.1.0/environment_win.yml --name tms_loco
   pip install https://github.com/simnibs/simnibs/releases/download/v4.1.0/simnibs-4.1.0-cp39-cp39-win_amd64.whl
   pip install pynibs
   ```

- Clone this repository or download the newest release of this script repository: https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto/-/releases
   ``` sh
   cd folder/where/to/download/this/repository
   git clone https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto 
   ```
- Alternatively, use the Windows Subsystem for Linux (WSL). 


## Test installation
### Quick test
SimNIBS and pyNIBS should be importable within `python` and you are ready to go:
``` python
import simnibs
import pynibs
```

### Extensive tests
We've set up another repository with test data and test scripts to test all scripts: [tmsloc_proto_test](https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto_tests). This is also a good start if you run into any problems with your data. 

# Usage instructions
Use the `tms_loco` environment to run the scripts in this repository, which has been downloaded into `./tmsloc_proto/`.
## Pre-experiment

### 01 - Create subject structure and subject description file 
``` sh
python scripts/01_create_subject_structure.py -f ./subject_0 
# edit subject_0.py to your needs, populate mri data folders
python subject0.py

```
The file `subject0.hdf5` now contains information about the subject, its MRI details, etc. To add or change information, edit the `subject0.py` file and run it to recreate the `subject0.hdf5` file.

### 02 - Construct 'mesh0' headmesh as defined in the subject object
This takes some hours to construct the headmesh with SimNIBS. Main output is `/mesh/subject0.msh`. 
``` sh
python scripts/02_make_msh_from_mri.py -s subject_0.hdf5 -m mesh0 
```

### 03 - Refine headmesh in region of interest
This refines a region of interest (ROI) within the headmesh as defined in `subject0.py`. Takes about 2 hours.
``` sh
python scripts/03_refine_mesh.py -s subject_0.hdf5 -m mesh0 -o mesh0_refinedM1
```

### 04 - Compute initial target for manual motor threshold hunting
By default, group level MRI FDI M1 coordinates are transformed into subject space. Use these coordinates as the initial cortical target for the data acquisition.
``` sh 
python scripts/04_get_initial_m1_coords.py -s subject_0.hdf5 -m mesh0_refinedM1
```

## Data acquisition
See our protocol paper for details on the experimental setup itself. 

## Post-experiment
### 05 - Combine experimental data in one dataset
This scripts combines coil positions/orientations and EMG responses and is explicitly written to be used with Localite TMS Navigator and CED Signal. EMG data is processed to calculate one motor evoked potential (MEP) per TMS pulse. This takes about 4-12 hours of computation time.
``` sh 
python scripts/05_merge_exp_data.py -s subject_0.hdf5 -m mesh0_refinedM1 -e m1 -d -r -p
```

### 06 - Calculate the induced electrical fields in the *midlayer_m1s1pmd* ROI
For each pulse delivered during the experiment its electric field is calculated based on the recorded coil position/orientation with SimNIBS.
``` sh
python scripts/06_calc_e.py -s subject_0.hdf5 -m mesh0_refinedM1 -e m1 -r midlayer_m1s1pmd -n 4 -a vn -p
```

### 07 - Fit electrical field data to MEPs and quantify goodness-of-fit in the *midlayer_m1s1pmd* ROI
Finally, for each element of the region of interest (like *voxel*, but finer resolution) the electrical field at that location is regressed onto the MEPs. Different types of functions can be fitted. This is done separateley for each muscle for that EMG has been recorded.
``` sh
python scripts/07_calc_r2.py -s subject_0.hdf5 -m mesh0_refinedM1 -e m1 -r midlayer_m1s1pmd -n 10 -f sigmoid4_log -i 10 -v
```

### 08 - Compute optimal coil position/orientation to stimulate FDI
We now know the precise cortical origin of the MEPs, so we can compute an optimal coil position/orientation to stimulate this spot:
``` sh
python scripts/08_calc_opt_coil_pos.py -s subject_0.hdf5 -m mesh0_refinedM1 -e m1 -n 4 -a vn -p -t subject0/results/.../FDI/.../r2_roi_data.hdf5 -c example_data/coils/MagVenture_MCF_B65_REF_highres.ccd.nii.gz
```
This script generates an instrument marker that you can use with the Localite TMS Navigator software to assess the resting motor threshold.

### 09 - Estimate the resting motor threshold from experimental data
If you don't want to run another experiment to assess the resting motor threshold (rMT) and if ±3 %MSO is fine, you can instead use this script to estimate it. This fits a sigmoidal function in the identified cortical spot (script 07) and determines the 50µV threshold. 

``` sh
python 09_estimate_rmt_from_data.py --fn_gof=r2_roi_data.hdf5 --fn_exp=experiment.hdf5 --fn_e=scaled_e.hdf5 --fn_e_opt=opt_field.hdf5 --muscle=FDI
```



    

