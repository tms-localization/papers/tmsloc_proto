# ROI examples

A region of interest (ROI) can be defined multiple ways. Define them in `create_subject_0.py`, rerun it and (re)run `02_make_msh_from_mri.py`.
This takes care of the following steps
- masking (in case of spherical ROIs)
- transformation from MNI to subject space (in case of MNI template)
- interpolating to GM-WM midlayer
- remeshing and cropping to midlayer
- adding roi to `subject_0.hdf5`
- creating `roi.hdf5` / `roi.xdmf` tuples in `meshfolder/roi/roiname/`


## 1. Spherical ROI from MNI coordinates

Add this to `create_subject_0.py` to construct a spherical ROI in MNI and transform to subject space:
``` python
roi['mesh0']['sphere_from_mni_coord'] = dict()
roi['mesh0']['sphere_from_mni_coord']['type'] = 'surface'
roi['mesh0']['sphere_from_mni_coord']['template'] = 'MNI'
roi['mesh0']['sphere_from_mni_coord']['info'] = 'Some important details'
roi['mesh0']['sphere_from_mni_coord']['gm_surf_fname'] = mesh['mesh0']['fn_lh_gm']
roi['mesh0']['sphere_from_mni_coord']['wm_surf_fname'] = mesh['mesh0']['fn_lh_wm']
roi['mesh0']['sphere_from_mni_coord']['midlayer_surf_fname'] = mesh['mesh0']['fn_lh_midlayer']
roi['mesh0']['sphere_from_mni_coord']['center'] = [-31, -22, 52]  # X, Y, Z MNI coordinates
roi['mesh0']['sphere_from_mni_coord']['radius'] = 20  # Radius im mm
roi['mesh0']['sphere_from_mni_coord']['fn_mask'] = f"roi/sphere_from_mni_coord/lh.mask_sphere"  # A FreeSurfer subject space surface overlay will be created in mesh folder. 
roi['mesh0']['sphere_from_mni_coord']['hemisphere'] = 'lh' 
```

## 2. Spherical ROI from subject coordinates
Same as above, but `'template'` key is `'subject'`.
``` python
roi['mesh0']['sphere_from_subject_coord'] = dict()
roi['mesh0']['sphere_from_subject_coord']['type'] = 'surface'
roi['mesh0']['sphere_from_subject_coord']['template'] = 'subject'
roi['mesh0']['sphere_from_subject_coord']['info'] = 'Some important details'
roi['mesh0']['sphere_from_subject_coord']['gm_surf_fname'] = mesh['mesh0']['fn_lh_gm']
roi['mesh0']['sphere_from_subject_coord']['wm_surf_fname'] = mesh['mesh0']['fn_lh_wm']
roi['mesh0']['sphere_from_subject_coord']['midlayer_surf_fname'] = mesh['mesh0']['fn_lh_midlayer']
roi['mesh0']['sphere_from_subject_coord']['center'] = [-31, -22, 52]  # X, Y, Z subject coordinates
roi['mesh0']['sphere_from_subject_coord']['radius'] = 20  # Radius im mm
roi['mesh0']['sphere_from_subject_coord']['fn_mask'] = f"roi/sphere_from_subject_coord/lh.mask_sphere"  # A FreeSurfer subject space surface overlay will be created in mesh folder. 
roi['mesh0']['sphere_from_subject_coord']['hemisphere'] = 'lh' 
```

## 3. ROI defined from MNI mask
Subject ROIs can also be created from an MNI mask.
``` python
roi['mesh0']['from_mni_nii'] = dict()
roi['mesh0']['from_mni_nii']['type'] = 'surface'
roi['mesh0']['from_mni_nii']['template'] = 'MNI'  
roi['mesh0']['from_mni_nii']['info'] = 'Very informative stuff here'
roi['mesh0']['from_mni_nii']['gm_surf_fname'] = mesh['mesh0']['fn_lh_gm']
roi['mesh0']['from_mni_nii']['wm_surf_fname'] = mesh['mesh0']['fn_lh_wm']
roi['mesh0']['from_mni_nii']['midlayer_surf_fname'] = mesh['mesh0']['fn_lh_midlayer']
roi['mesh0']['from_mni_nii']['fn_mask'] = f"roi/from_mni_nii/lh.mask_from_nii"  # A FreeSurfer subject space surface overlay will be created in mesh folder. 
roi['mesh0']['from_mni_nii']['fn_mask_nii'] = f"/path/to/existing/mask.nii"  # Existing nifti image with mask in MNI space
roi['mesh0']['from_mni_nii']['hemisphere'] = 'lh'  
```

## 4. ROI defined from subject mask
Same as above, but `'template'` key is `'subject'`.
``` python
roi['mesh0']['from_subject_nii'] = dict()
roi['mesh0']['from_subject_nii']['type'] = 'surface'
roi['mesh0']['from_subject_nii']['template'] = 'subject'
roi['mesh0']['from_subject_nii']['info'] = 'Very informative stuff here'
roi['mesh0']['from_subject_nii']['gm_surf_fname'] = mesh['mesh0']['fn_lh_gm']
roi['mesh0']['from_subject_nii']['wm_surf_fname'] = mesh['mesh0']['fn_lh_wm']
roi['mesh0']['from_subject_nii']['midlayer_surf_fname'] = mesh['mesh0']['fn_lh_midlayer']
roi['mesh0']['from_subject_nii']['fn_mask'] = f"roi/from_subject_nii/lh.mask_from_nii"  # A FreeSurfer subject space surface overlay will be created in mesh folder. 
roi['mesh0']['from_subject_nii']['fn_mask_nii'] = f"/path/to/existing/subject_mask.nii"  # Existing nifti image with mask in subject space
roi['mesh0']['from_subject_nii']['hemisphere'] = 'lh'  
```

## 5. ROIs defined from fsaverage mask
``` python
rois['mesh0']['from_fsaverage'] = dict()} 
rois['mesh0']['from_fsaverage']['type'] = 'surface'
rois['mesh0']['from_fsaverage']['template'] = 'fsaverage'
rois['mesh0']['from_fsaverage']['info'] = 'really import information about this ROI here'
rois['mesh0']['from_fsaverage']['hemisphere'] = "lh"
rois['mesh0']['from_fsaverage']['midlayer_surf_fname'] = mesh['mesh0']['fn_lh_midlayer']
rois['mesh0']['from_fsaverage']['fn_mask'] = f"roi/from_fsaverage/mask_lefthandknob_M1S1PMd.mgh"
rois['mesh0']['from_fsaverage']['fn_mask_avg'] = os.path.join(os.path.split(subject_folder)[0], 
						                           'ROI', 'lefthandknob_M1S1PMd.overlay')
```
