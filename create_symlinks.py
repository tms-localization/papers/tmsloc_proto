"""
Create symlinks to the protocols example_data dir in the local git repository folder.
"""

import os
import pathlib

# Where am I?
basefolder = pathlib.Path(__file__).parent.absolute()

# Where is the protocols paper data set?
example_data = "/data/pt_01756/studies/regression_protocols_paper/example_data"

# Which folders to link?
folders = ["coil", "exp", "mesh", "mri", "opt", "results", "roi", "software"]
if not os.path.exists(f"{basefolder}/subject_0/"):
    os.makedirs(f"{basefolder}/subject_0/")
for folder in folders:
    cmd = f"ln -s {example_data}/subject_0/{folder} {basefolder}/subject_0/{folder}"
    os.popen(cmd).read()
    
folders = [ "roi", "software"]
for folder in folders:
    cmd = f"ln -s {example_data}/{folder} {basefolder}/{folder}"
    os.popen(cmd).read()
