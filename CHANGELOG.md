Version 0.2024.8 (2024/08/02)
-----------------------------
- Support of SimNIBS 4.1.0 (with FreeSurfer reconstruction), pyNIBS v0.2023.4
- Cortical layers support
- Live FEM

Version 0.2023.8 (2023/08/24)
-----------------------------
- Support of SimNIBS 4.0.1, pyNIBS v0.2023.8
- 06_calc_e.py: Remove monkeypatched calc_e interpolation
- 06_calc_e.py: Fixed out-of-memory during e-field calculations

Version 0.2023.3 (2023/03/20)
------------------------------
- Some smaller bugfixes, including updated electrical field interpolation to improve accuracy.
- Compatible with pyNIBS v0.2023.3

Version 0.2022.12 (2022/12/08) 
------------------------------
- Many improvements to increase stability, some bugfixes, and some SimNIBS 3.2.6 bug workarounds.
- SimNIBS 4.0 compatibility added
  - ``02_make_msh_from_mri_simnibs4.py`` added
  - all other scripts adjusted to allow SimNIBS 4.0 use
  - this introduces a new approach to refine the mesh around the ROI (see create_subject.py)
- Updated ``exp/`` and ``results/`` folder structure:
  - ``experiment.hdf5`` is now mesh dependent due to differences in coil correction
  - ``meshID`` and ``roiID`` are now integrated in folder subfolders:
    - ``exp/m1`` -> ``exp/mesh_meshID/m1``
    - ``results/electrical_field/m1`` -> ``results/electrical_field/mesh_meshID/m1``
    - ``results/r2/m1`` -> ``results/r2/mesh_meshID/roi_roiIDm1``
    - This allows to have mesh- and ROI-dependent analyses pipelines.
    - The example dataset has been updated accordingly.
- compatible with pyNIBS v0.2022.12    

Version 0.2 (2022/03/16)
------------------------
Some updates to reflect changes in the 2nd review round.

Version 0.1 (2022/12/22)
------------------------
Initial push.