#!/usr/bin/env python

"""
This script loads a pyNIBS subject object and calls simnibs to construct the headmesh.
"""

import os
import sys
import glob
import time
import warnings

import h5py
import shutil
import nibabel
import pynibs
import argparse
import datetime
import numpy as np
from simnibs import SIMNIBSDIR, transformations


def logprint(text):
    """Just a wrapper to print and write text to a logfile."""
    logfile.write(f'{text}\n')
    print(text)


time_mesh = 0
time_roi = 0
time_dwi = 0

# Get command line arguments
parser = argparse.ArgumentParser(description='Creates a FEM mesh from MRI data\n')
parser.add_argument('-s', '--fn_subject', help='Path to *.hdf5-file of subject', required=True)
parser.add_argument('-m', '--mesh_idx', help='mesh_idx', required=True)

args = parser.parse_args()

# print simulation info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)

print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

fn_subject = os.path.abspath(args.fn_subject)
mesh_idx = args.mesh_idx

# load subject info
subject = pynibs.load_subject(fn_subject)
mri_idx = subject.mesh[mesh_idx]["mri_idx"]

# set approach specific options
approach = subject.mesh[mesh_idx]["approach"]

if approach == "headreco":
    vertex_density = subject.mesh[mesh_idx]["vertex_density"]
    numvertices = None
elif approach == "mri2mesh":
    numvertices = subject.mesh[mesh_idx]["numvertices"]
    vertex_density = None
else:
    raise NotImplementedError(f"Approach {approach} not implemented. Use 'headreco' or 'mri2mesh'")

# check if 'mri' field exists
if not hasattr(subject, 'mri'):
    print(f'\'MRI\' attribute missing in {fn_subject}. Quitting.')
    sys.exit(1)

mesh_folder = subject.mesh[mesh_idx]["mesh_folder"]
if not os.path.exists(mesh_folder):
    os.makedirs(mesh_folder)

t1_path = subject.mri[mri_idx]['fn_mri_T1']
t2_path = subject.mri[mri_idx]['fn_mri_T2']
bvec = subject.mri[mri_idx]['fn_mri_DTI_bvec']
bval = subject.mri[mri_idx]['fn_mri_DTI_bval']
dti_path = subject.mri[mri_idx]['fn_mri_DTI']

if 'fn_mri_DTI_rev' in subject.mri[mri_idx].keys():
    dti_rev_path = subject.mri[mri_idx]['fn_mri_DTI_rev']
else:
    dti_rev_path = ''
if dti_rev_path is None:
    dti_rev_path = ''

if t2_path is None:
    t2_path = ''

# check if FreeSurfer env is available
try:
    fsaverage_dir = os.path.join(f"{os.environ['SUBJECTS_DIR']}", "fsaverage")
except KeyError:
    fsaverage_dir = ''
    warnings.warn("FreeSurfer not found")
    time.sleep(2)

# assert os.path.exists(fsaverage_dir), f"{fsaverage_dir} does not exist. Is FreeSurfer installed?"

logfile = open(os.path.join(mesh_folder, f"info_{datetime.datetime.now():%Y%m%d_%H%M%S}.log"), "w")

# create mesh from mri if not already done
if not os.path.isfile(subject.mesh[mesh_idx]["fn_mesh_msh"]):
    # open logfile

    logfile.write(f"Started make_msh_from_mri.py on {datetime.datetime.now().strftime('%Y-%m-%d %H:%M')}\n")
    logfile.write("================================================\n")
    logfile.write(f"Subject object file: {fn_subject}\n")
    logfile.write(f"mri_idx:             {mri_idx}\n")
    logfile.write(f"mesh_idx:            {mesh_idx}\n")
    logfile.write(f"approach:            {approach}\n")
    if approach == "mri2mesh":
        logfile.write(f"numvertices:         {numvertices}\n")
    else:
        logfile.write(f"vertex_density:      {vertex_density}\n")
    logfile.write(f"fn_mri_T1:           {subject.mri[mri_idx]['fn_mri_T1']}\n")
    logfile.write(f"fn_mri_T2:           {subject.mri[mri_idx]['fn_mri_T2']}\n")
    logfile.write(f"fn_mri_DTI_bvec:     {subject.mri[mri_idx]['fn_mri_DTI_bvec']}\n")
    logfile.write(f"fn_mri_DTI_bval:     {subject.mri[mri_idx]['fn_mri_DTI_bval']}\n")
    logfile.write(f"fn_mri_DTI:          {subject.mri[mri_idx]['fn_mri_DTI']}\n\n")

    os.chdir(mesh_folder)

    if approach == "mri2mesh":
        command = f"mri2mesh --all --numvertices={str(numvertices)} " \
            f"{str(subject.id)} {str(t1_path)} {str(t2_path)}"
    else:  # approach == "headreco":
        command = f"{sys.executable} {SIMNIBSDIR}/cli/headreco.py all -v {vertex_density} " \
            f"{subject.id} {t1_path} {t2_path}"

    logfile.write(f"Generating head model using {approach}\n")
    logfile.write("================================\n")
    logfile.write(f" > {command}\n")

    time_start_mesh = time.time()
    os.popen(command).read()
    time_mesh = time.time() - time_start_mesh
else:
    print(f"Mesh {subject.mesh[mesh_idx]['fn_mesh_msh']} already exists. Skipping")

# rename .msh file to user specific filename
mesh_fn = os.path.join(mesh_folder, subject.id + ".msh")
if os.path.exists(mesh_fn):
    os.rename(mesh_fn, subject.mesh[mesh_idx]["fn_mesh_msh"])
else:
    FileNotFoundError(f".msh file not created successfully. Please check {approach} log file for errors "
                      f"({os.path.join(mesh_folder, 'm2m_' + subject.id, approach + '_log.html')}).")

# create subject specific roi mask files
#############################################################################################################
# transform surfaces from .gii format to FreeSurfer surfaces
if approach == "headreco":
    fn_fs_surf = os.path.join(mesh_folder, f"fs_{subject.id}", "surf")
    fn_headreco_surf = os.path.join(mesh_folder, f"m2m_{subject.id}", "segment", "cat", "surf")
    fn_gii_files = glob.glob(os.path.join(fn_headreco_surf, "*.gii"))

    # create fs_ folder since it does not exist for headreco
    if not os.path.exists(fn_fs_surf):
        os.makedirs(fn_fs_surf)

    # convert all surfaces from .gii format to FreeSurfer surfaces
    for fn_gii in fn_gii_files:
        fn_trg = os.path.join(fn_fs_surf, os.path.split(os.path.splitext(os.path.splitext(fn_gii)[0])[0])[1])
        print(f" > Converting {fn_gii} file to {fn_trg}")
        s = nibabel.load(fn_gii)
        faces = s.get_arrays_from_intent('NIFTI_INTENT_TRIANGLE')[0].data
        coords = s.get_arrays_from_intent('NIFTI_INTENT_POINTSET')[0].data
        nibabel.freesurfer.io.write_geometry(filepath=fn_trg,
                                             coords=coords,
                                             faces=faces)

    # TODO: For delta!=0.5 -> create WM and GM surfaces from midlayer data and cortical thickness

# Create ROI
#############################################################################################################
if mesh_idx in subject.roi.keys():
    for roi_name, roi in subject.roi[mesh_idx].items():
        time_start_roi = time.time()

        # only run ROI things if fn_mask does not exist yet
        if not os.path.exists(os.path.join(mesh_folder, roi['fn_mask'])):

            # Default to FreeSurfer ROI creation to keep backwards compat
            if roi['template'] in [None, 'fsaverage', 'freesurfer']:
                if os.name == 'nt':  # 'nt' is windows.
                    warnings.warn(f'Cannot create FreeSurfer based ROI {roi_name} with Windows. Skipping ROI.')
                    continue
                roi_dir = os.path.join(mesh_folder, "roi", str(roi_name))

                os.makedirs(roi_dir, exist_ok=True)

                logprint(f"Creating ROI '{roi_name}'")
                logprint(f"===============")

                # write bash script with the FreeSurfer commands
                f = open(os.path.join(roi_dir, "get_mask.sh"), "w")
                f.write(f"export SUBJECTS_DIR='{mesh_folder}'\n")
                f.write(f"cd {mesh_folder}\n")
                f.write(f"rm -f fsaverage\n")

                f.write(f"ln -s {fsaverage_dir} 'fsaverage'\n")
                f.write(
                    f"mri_surf2surf --srcsubject 'fsaverage' "
                    f"--srcsurfval '{subject.roi[mesh_idx][roi_name]['fn_mask_avg']}' "
                    f"--trgsubject 'fs_'{subject.id} "
                    f"--trgsurfval {os.path.join(mesh_folder, roi['fn_mask'])} "
                    f"--hemi {roi['hemisphere']}\n")
                f.close()

                # run FreeSurfer script
                command = f"sh {os.path.join(roi_dir, 'get_mask.sh')}"
                print(f"> Running '{command}'")
                os.popen(command).read()

            elif roi['template'] in ['MNI', 'subject']:
                roi_folder = os.path.join(subject.subject_folder, 'mesh', mesh_idx, 'roi', roi_name)

                fn_roi_sub_thresh_nii = os.path.join(roi_folder, 'roi_sub_thresh.nii.gz')
                os.makedirs(roi_folder, exist_ok=True)
                m2m_dir = os.path.join(subject.subject_folder, 'mesh', mesh_idx, f"m2m_{subject.id}")

                if roi['center'] is not None and roi['radius'] is not None:
                    # get hemisphere
                    if 'hemisphere' in roi and roi['hemisphere'] is not None:
                        hem = roi['hem']
                    else:
                        if roi['center'][0] < 0:
                            hem = 'lh'
                        else:
                            hem = 'rh'
                        print(f'Hemisphere not provided. Guessing {hem} from coordinates')

                    # create spherical ROI from MNI or subject template
                    assert roi['fn_mask_nii'] is None, f"ROI '{roi_name}: Only provide 'center' + 'radius' or 'nii_mask"

                    # get sphere
                    if roi['template'] == 'MNI':
                        nii_template = None
                        fn_roi_mni_nii = os.path.join(roi_folder, 'roi_mni.nii.gz')
                        fn_roi_sub_nii = os.path.join(roi_folder, 'roi_sub.nii.gz')
                    elif roi['template'] == 'subject':
                        nii_template = os.path.join(subject.subject_folder, 'mesh', mesh_idx,
                                                    subject.mesh[mesh_idx]['fn_mri_conform'])
                        fn_roi_mni_nii = os.path.join(roi_folder, 'roi_sub.nii.gz')
                        fn_roi_sub_nii = os.path.join(roi_folder, 'roi_sub.nii.gz')
                    else:
                        raise ValueError

                    pynibs.get_sphere_in_nii(center=roi['center'], radius=roi['radius'], nii=nii_template,
                                             out_fn=fn_roi_mni_nii)

                    if roi['template'] == 'MNI':
                        # warp to subject, yields .nii
                        transformations.warp_volume(
                            fn_roi_mni_nii, m2m_dir, fn_roi_sub_nii,
                            transformation_direction='mni2subject')

                    # clean roi
                    sub_nii_thres = pynibs.clean_roi(fn_roi_sub_nii)

                    # create surface files
                    pynibs.nii2msh(mesh_fn, m2m_dir, sub_nii_thres, roi_folder, hem, out_fsaverage=False,
                                   roi_name=roi_name)

                    # rename output files
                    os.rename(os.path.join(roi_folder, f"{hem}.mesh.central.{roi_name}"),
                              os.path.join(mesh_folder, roi['fn_mask']))

                elif roi['fn_mask_nii'] is not None:
                    # create mesh ROI from MNI or subject nii
                    fn_mask_nii = roi['fn_mask_nii']
                    if not roi['fn_mask_nii'].startswith(os.sep):
                        fn_mask_nii = os.path.join(mesh_folder, fn_mask_nii)

                    hem = roi['hemisphere']
                    if roi['template'] == 'MNI':
                        # warp to subject, yields .nii
                        transformations.warp_volume(
                            fn_mask_nii, m2m_dir, fn_mask_nii.replace('.nii', '_sub.nii'),
                            transformation_direction='mni2subject')

                        fn_mask_nii = pynibs.clean_roi(fn_mask_nii.replace('.nii', '_sub.nii'))

                    # create surface files
                    pynibs.nii2msh(mesh_fn, m2m_dir, fn_mask_nii, roi_folder, hem, out_fsaverage=False,
                                   roi_name=roi_name)

                    os.rename(os.path.join(roi_folder, f"{hem}.mesh.central.{roi_name}"),
                              os.path.join(mesh_folder, roi['fn_mask']))

                # remove some files
                fns = [
                    os.path.join(roi_folder, "mesh_fsavg.msh.opt"),
                    os.path.join(roi_folder, "mesh_fsavg.msh"),
                    os.path.join(roi_folder, "mesh_central.msh"),
                    os.path.join(roi_folder, f"rh.mesh.fsavg.{roi_name}"),
                    os.path.join(roi_folder, f"rh.mesh.central.{roi_name}"),
                    os.path.join(roi_folder, "mesh_central.msh.opt"),
                    os.path.join(roi_folder, f"lh.mesh.fsavg.{roi_name}"),
                    os.path.join(roi_folder, f"lh.mesh.central.{roi_name}"),
                    os.path.join(roi_folder, "rh.central"),
                    os.path.join(roi_folder, "lh.central"),
                    os.path.join(roi_folder, "mesh.msh")
                ]
                for fn in fns:
                    try:
                        os.unlink(fn)
                    except:
                        pass

            else:
                raise ValueError(f"ROI template '{roi['template']} unknown.'")
        else:
            logprint(f"ROI '{roi_name}' already exists.")
        time_stop_roi = time.time()
        time_roi = time_stop_roi - time_start_roi

os.chdir(mesh_folder)

# # map to average subject
# if not os.path.isfile(os.getcwd() + os.sep + 'mask_' + subject.id + '.mgh'):
#     os.putenv('SUBJECTS_DIR', os.getcwd())
#     bash_call("""mri_surf2surf --srcsubject "fsavarage" --srcsurfval""" +
#               """ "/data/pt_01756/masks/lefthandknob_M1S1.overlay" """ + """--trgsubject 'fs_""" + subject.id +
#               """'""" + """ --trgsurfval """ + """'""" + os.getcwd() + """/mask_""" + subject.id + """.mgh""" +
#               """'""" + """ --hemi """ + """ 'lh' """)

# create *.hdf5 mesh file (incl. ROIs) from *.msh file
#############################################################################################################
logprint("Transforming mesh from .msh to .hdf5")
logprint(f"{'=' * 36}")

skip_msh2hdf5 = False
fn_mesh_hdf5 = subject.mesh[mesh_idx]['fn_mesh_hdf5']
if os.path.exists(fn_mesh_hdf5):
    skip_msh2hdf5 = True
    logprint(f" > {fn_mesh_hdf5} already exists. Checking existence of ROIs ...")

    with h5py.File(fn_mesh_hdf5, "r") as f:
        if mesh_idx in subject.roi.keys():
            for roi_idx in subject.roi[mesh_idx].keys():
                try:
                    _ = f[f"roi_surface/{roi_idx}"]
                    logprint(f" > roi_surface/{roi_idx} exists")

                except KeyError:
                    logprint(f" > roi_surface/{roi_idx} does not exist")
                    skip_msh2hdf5 = False
                    break
else:
    skip_msh2hdf5 = False

if not skip_msh2hdf5:
    logprint(f" > Transforming .mesh to .hdf5 and adding ROIs")
    pynibs.msh2hdf5(subject=subject, fn_msh=subject.mesh[mesh_idx]['fn_mesh_msh'], mesh_idx=mesh_idx)
    logprint(f" > Writing file: {fn_mesh_hdf5}\n")

    # create .xdmf file for visualization in paraview
    pynibs.write_xdmf(fn_mesh_hdf5, overwrite_xdmf=True, verbose=True)
    logprint(f" > Created .xdmf file: {os.path.splitext(fn_mesh_hdf5)[0] + '.xdmf'}")

else:
    logprint(" > Skipping transformation from .msh to .hdf5 (already exists and all ROIs are included)")

# create geo.hdf5 files for ROIs in mesh/mesh_idx/roi/roi_idx subfolders
#############################################################################################################
roi = pynibs.load_roi_surface_obj_from_hdf5(fname=subject.mesh[mesh_idx]['fn_mesh_hdf5'])

if mesh_idx in subject.roi.keys():
    for roi_idx in subject.roi[mesh_idx].keys():
        fn_geo_roi = os.path.join(mesh_folder, "roi", roi_idx, "geo.hdf5")

        if not os.path.exists(fn_geo_roi):
            logprint(f" > Creating geo.hdf5 for ROI #{roi_idx}: {fn_geo_roi}")

            if not os.path.exists(os.path.split(fn_geo_roi)[0]):
                os.makedirs(os.path.split(fn_geo_roi)[0])

            pynibs.write_geo_hdf5_surf(out_fn=fn_geo_roi,
                                       points=roi[roi_idx].node_coord_mid,
                                       con=roi[roi_idx].node_number_list,
                                       replace=True,
                                       hdf5_path='/mesh')
            pynibs.write_xdmf(fn_geo_roi)

# determine anisotropic conductivity tensors
#############################################################################################################
d2c_folder = os.path.join(mesh_folder, f'd2c_{subject.id}')
create_conductivity_tensors = bvec is not None and \
                              bval is not None and \
                              dti_path is not None \
                              and not (os.path.exists(os.path.join(subject.mesh[mesh_idx]["mesh_folder"],
                                                                   subject.mesh[mesh_idx]['fn_tensor_vn']))
                                       and os.path.exists(os.path.join(d2c_folder,
                                                                       "first_ev_for_check.hdf5"))
                                       )

if create_conductivity_tensors:
    if os.name == 'nt':  # 'nt' is windows
        warnings.warn('Cannot create diffusion tensors on Windows.')
    else:
        time_start_dwi = time.time()

        # fix .bvals to 0:
        assert os.path.exists(subject.mri[mri_idx]['fn_mri_DTI_bval']), \
            f"Can't find {subject.mri[mri_idx]['fn_mri_DTI_bval']}"

        with open(subject.mri[mri_idx]['fn_mri_DTI_bval'], 'r+') as f:
            f.seek(0)
            bvals = f.read()

            bvals = bvals.replace('\n', '')

            if bvals[-1] == " ":
                bvals = bvals[:-1]

            bvals = np.array(bvals.split(" ")).astype(int)

            if 0 not in bvals:
                min_val = np.min(bvals)
                assert min_val <= 10
                logprint(f"No b0 found in {subject.mri[mri_idx]['fn_mri_DTI_bval']}. Changing {min_val} to 0.")
                bvals[bvals == min_val] = 0
                bvals = " ".join(bvals.astype(str).tolist())
                shutil.copy(subject.mri[mri_idx]['fn_mri_DTI_bval'], subject.mri[mri_idx]['fn_mri_DTI_bval'] + '.org')
                f.seek(0)
                f.write(bvals)

        command = f"{SIMNIBSDIR}/bash_modules/dwi2cond --all --eddy --readout=" + \
                  str(subject.mri[mri_idx]['dti_readout_time']) + \
                  " --phasedir=" + str(subject.mri[mri_idx]['dti_phase_direction']) + " " + \
                  str(subject.id) + " " + str(dti_path) + " " + str(bval) + " " + str(bvec) + " " + str(dti_rev_path)

        logprint("Creating anisotropic conductivity tensors from DWI data")
        logprint("=======================================================")
        logprint(f"Running {command}")

        os.putenv('SUBJECTS_DIR', os.getcwd())

        if not os.path.exists(os.path.join(mesh_folder, subject.mesh[mesh_idx]["fn_tensor_vn"])):
            os.popen(command).read()
            logprint(f" > Created files in folder {os.path.join(mesh_folder, 'd2c_' + str(subject.id))}\n")

        else:
            logprint(f" > Anisotropy files already exist ... skipping to create "
                     f"{subject.mesh[mesh_idx]['fn_tensor_vn']}")

        time_stop_dwi = time.time()
        time_dwi = time_stop_dwi - time_start_dwi

        # convert to first eigenvalues to .hdf5 for visualization in Paraview
        pynibs.msh2hdf5(subject=subject,
                        mesh_idx=mesh_idx,
                        fn_msh=os.path.join(mesh_folder, 'd2c_' + str(subject.id), "first_ev_for_check.msh"),
                        skip_roi=True,
                        include_data=True)

if approach == "mri2mesh":
    logprint(f"\nCreated mesh #{mesh_idx} using {approach} from mri {mri_idx} with {numvertices} vertices per surface")
else:
    logprint(f"\nCreated mesh #{mesh_idx} using {approach} from mri {mri_idx} with vertex density {vertex_density}")

logprint(f"Time: Creating mesh: {time_mesh:6.2f} secs")
logprint(f"Time:  Creating ROI: {time_roi:6.2f} secs")
if create_conductivity_tensors:
    logprint(f"Time:   Creating FA: {time_dwi:6.2f} secs")
logprint("============ DONE ============")

logfile.close()
