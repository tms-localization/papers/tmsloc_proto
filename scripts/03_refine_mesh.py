#!/usr/bin/env python

"""
Refine mesh in spherical ROI.
"""

import os
import sys
import shutil
import trimesh
import pynibs
import argparse
import numpy as np
import nibabel as nib
import simnibs

if not simnibs.__version__.startswith('3'):
    raise ValueError('This script only runs with SimNIBS 3.2.6')

import simnibs.msh.hmutils as hmu
import simnibs.msh.transformations as transformations

from glob import glob
from inspect import getsourcefile
from simnibs import SIMNIBSDIR
from simnibs.msh import mesh_io
from simnibs.msh.hmutils import decouple_surfaces
from simnibs.msh.headreco import decouple_wm_gm_ventricles

parser = argparse.ArgumentParser(description='Refines an existent FEM mesh')
parser.add_argument('-s', '--fn_subject', help='Path to *.hdf5-file of subject', required=True)
parser.add_argument('-m', '--mesh_idx_in', help='input mesh_idx', required=True)
parser.add_argument('-o', '--mesh_idx_out', help='output mesh_idx', required=True)

args = parser.parse_args()

fn_subject = os.path.abspath(args.fn_subject)
mesh_idx_in = args.mesh_idx_in
mesh_idx_out = args.mesh_idx_out
verbose = True

fn_make_msh_from_mri = os.path.join(os.path.split(os.path.abspath(getsourcefile(lambda: 0)))[0],
                                    "02_make_msh_from_mri.py")

# load subject info
subject = pynibs.load_subject(fn_subject)

refine_domains = subject.mesh[mesh_idx_out]["refine_domains"]
smooth_domains = subject.mesh[mesh_idx_out]["smooth_domains"]

dom_ids = {"wm": "1", "gm": "2", "csf": "3", "bone": "4", "skin": "5"}
suffixes = {"wm": "", "gm": "", "csf": "", "bone": "", "skin": ""}

domain_id_refined = [dom_ids[key] for key in refine_domains]
domain_id_not_refined = [dom_ids[key] for key in dom_ids if dom_ids[key] not in domain_id_refined]

radius = subject.mesh[mesh_idx_out]["radius"]
center = subject.mesh[mesh_idx_out]["center"]
element_size = subject.mesh[mesh_idx_out]["element_size"]
m2m_folder = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id)
fn_msh = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + ".msh")
fn_msh_gmsh = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + "_gmsh.msh")

# create folders and copy original mesh
if not os.path.exists(subject.mesh[mesh_idx_out]["mesh_folder"]):
    os.makedirs(subject.mesh[mesh_idx_out]["mesh_folder"])

# create folders and copy original mesh
if not os.path.exists(subject.mesh[mesh_idx_out]["mesh_folder"]):
    os.makedirs(subject.mesh[mesh_idx_out]["mesh_folder"])

print("copying files ...")
if os.path.exists(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id)):
    shutil.rmtree(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id))

shutil.copytree(src=os.path.join(subject.mesh[mesh_idx_in]["mesh_folder"], "m2m_" + subject.id),
                dst=os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id))

if os.path.exists(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fs_" + subject.id)):
    shutil.rmtree(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fs_" + subject.id))

shutil.copytree(src=os.path.join(subject.mesh[mesh_idx_in]["mesh_folder"], "fs_" + subject.id),
                dst=os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fs_" + subject.id))

if os.path.exists(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fsaverage")):
    os.unlink(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fsaverage"))

try:
    shutil.copy2(src=os.path.join(subject.mesh[mesh_idx_in]["mesh_folder"], "fsaverage"),
                 dst=os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "fsaverage"), follow_symlinks=False)
except FileExistsError:
    pass

if not os.path.exists(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + "_T1fs_conform.nii.gz")):
    shutil.copy2(src=os.path.join(subject.mesh[mesh_idx_in]["mesh_folder"], subject.id + "_T1fs_conform.nii.gz"),
                 dst=os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + "_T1fs_conform.nii.gz"))

if not os.path.exists(fn_msh):
    # smooth surfaces
    #############################################################################################################
    print("Smoothing surfaces")
    print("==================")
    # Currently we only allow the smoothing of the skin due to intersection issues with
    # internal air and the ventricles when the internal strutcures are smoothed.
    # This script is fully prepared to handle multiple sufaces for smoothing if the issues/bugs
    # with the decoupling of the smoothed surfaces (below) are resolved.
    not_allowed = ["wm", "gm", "csf", "bone"]
    if any((tissue in not_allowed for tissue in smooth_domains)):
        print("WARNING: Smoothing of the surfaces '" + ', '.join(not_allowed) + "' currently not supported. Skipping.")
        for domain_i in not_allowed:
            try:  # exception if element not present
                smooth_domains.remove(domain_i)
            except:
                pass

	for smooth_domain in smooth_domains:
		print("> '" + smooth_domain + "'")
		suffix = "_smooth"
		in_fname = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"],
		                        f"m2m_{subject.id}",
		                        f"{smooth_domain}{suffixes[smooth_domain]}.stl")
		out_fname = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"],
		                         f"m2m_{subject.id}",
		                         f"{smooth_domain}{suffixes[smooth_domain]}{suffix}.stl")

        surface = trimesh.load(in_fname)
        lamb = 0.1
        nu = -0.1
        iterations = 10
        if smooth_domain == "skin":
            lamb = 0.6
            nu = -0.1
            iterations = 50
        elif smooth_domain == "bone":
            lamb = 0.3
            nu = -0.6
            iterations = 10
        elif smooth_domain == "csf":
            lamb = 0.8
            nu = -0.8
            iterations = 50
        elif smooth_domain == "gm":
            lamb = 0.1
            nu = -0.1
            iterations = 10
        elif smooth_domain == "wm":
            lamb = 0.1
            nu = -0.1
        else:
            print("Unknown tissue selected for smoothing. Using default params (lambda=0.5, mu=-0.5, it=10)")

		trimesh.smoothing.filter_taubin(mesh=surface, lamb=lamb, nu=nu, iterations=iterations)

		surface.export(out_fname, file_type='stl_ascii')
		# set the number of shells to an arbitrary large value (1 MM) in case we deal
		# with structures that exhibit multiple detached components.
		# Using the maximum integer 'sys.maxsize' for this migh cause problems if MeshFix cannot
		# handle such large number (9223372036854775807) resulting in an overflow.
		command = f"meshfix {out_fname} -a 2.0 -u 1 -q --shells {str(1000000)} " \
		          f"--stl -o {out_fname}"
		os.popen(command).read()

		suffixes[smooth_domain] += suffix

	# refine surfaces of domains
	#############################################################################################################
	print("Refining surfaces")
	print("=================")

	for d in refine_domains:
		suffix = "_refined"
		in_fname = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id, d + suffixes[d] + ".stl")
		out_fname = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id,
				         d + suffixes[d] + suffix + ".stl")

		pynibs.refine_surface(fn_surf=in_fname,
				           fn_surf_refined=out_fname,
				           center=subject.mesh[mesh_idx_out]["center"],
				           radius=subject.mesh[mesh_idx_out]["radius"],
				           verbose=verbose,
				           repair=True,
				           remesh=True)

		suffixes[d] += suffix

	# modify .geo file of gmsh
	#############################################################################################################
	fn_geo = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "m2m_" + subject.id, subject.id + ".geo")

	# read .geo file
	with open(fn_geo, "r") as f:
		geo = f.readlines()

	# change mesh algorithm and add options
	new_options = ["Mesh.MeshSizeFromPoints = 0;\n",
			   "Mesh.MeshSizeFromCurvature = 0;\n",
			   "Mesh.MeshSizeExtendFromBoundary = 0;\n"]

	mesh_in_line = False

	for i_line, line in enumerate(geo):
		if "Mesh.Algorithm3D" in line:
		geo[i_line] = "Mesh.Algorithm3D=1;\n"
		if "Mesh." in line:
		mesh_in_line = True
		else:
		if mesh_in_line:
			for i_opt, opt in enumerate(new_options):
			geo.insert(i_line + i_opt, opt)
			break

	# read and modify surfaces
	surfaces = []
	for i_line, line in enumerate(geo):
		if "Merge" in line:
		start_idx = line.find("\"")
		stop_idx = start_idx + line[start_idx + 1:].find("\"")
		surface = os.path.splitext(os.path.split(line[(start_idx + 1):(stop_idx - 1)])[1])[0]

		surfaces.append(surface)

		geo[i_line] = f"Merge \"{os.path.join(m2m_folder, surface + suffixes.get(surface, ''))}.stl\";\n"

	# set mesh options
	mesh_options = [f"\n\n// Mesh refinement\n",
					f"Field[1] = Ball;\n",
					f"Field[1].Radius = {radius};\n",
					f"Field[1].Thickness = {radius / 3};\n",
					f"Field[1].VIn = {element_size};\n",
					f"Field[1].VOut = 3;\n",
					f"Field[1].XCenter = {center[0]};\n",
					f"Field[1].YCenter = {center[1]};\n",
					f"Field[1].ZCenter = {center[2]};\n",
					f"\n",
					f"Field[2] = Restrict;\n",
					f"Field[2].InField = 1;\n",
					f"Field[2].VolumesList = {{{','.join(domain_id_refined)}}};\n",
					f"\n"]

	if domain_id_not_refined:
		mesh_options = mesh_options + [f"Field[3] = Ball;\n",
								       f"Field[3].Radius = {radius};\n",
								       f"Field[3].Thickness = {radius / 3};\n",
								       f"Field[3].VIn = 1.5;\n",
								       f"Field[3].VOut = 3;\n",
								       f"Field[3].XCenter = {center[0]};\n",
								       f"Field[3].YCenter = {center[1]};\n",
								       f"Field[3].ZCenter = {center[2]};\n",
								       f"\n",
								       f"Field[4] = Restrict;\n",
								       f"Field[4].InField = 3;\n",
								       f"Field[4].VolumesList = {{{','.join(domain_id_not_refined)}}};\n",
								       f"\n",
								       f"Field[5] = Min;\n",
								       f"Field[5].FieldsList = {{2, 4}};\n",
								       f"\n",
								       f"Background Field = 5;\n"]
	else:
		mesh_options = mesh_options + ["Background Field = 2;\n"]

	geo_new = geo + mesh_options

	# write the .geo file
	fn_geo_refined = os.path.splitext(fn_geo)[0] + "_refined.geo"

	with open(fn_geo_refined, "w") as f:
		f.write(("").join(geo_new))

	# test if refined and remeshed surfaces are intersecting, if so decouple
	#############################################################################################################
	print("Checking if surfaces are overlapping ...")
	wm_surf = os.path.join(m2m_folder, "wm" + suffixes["wm"] + ".stl")
	gm_surf = os.path.join(m2m_folder, "gm" + suffixes["gm"] + ".stl")
	csf_surf = os.path.join(m2m_folder, "csf" + suffixes["csf"] + ".stl")
	bone_surf = os.path.join(m2m_folder, "bone" + suffixes["bone"] + ".stl")
	skin_surf = os.path.join(m2m_folder, "skin" + suffixes["skin"] + ".stl")
	ventricles_surf = os.path.join(m2m_folder, "ventricles" + suffixes.get("ventricles", '') + ".stl")
	air_decoupled_surf = os.path.join(m2m_folder, "air_outer_decoupled.stl")
	air_relabelled_surf = os.path.join(m2m_folder, "air_outer_relabel.stl")
	air_inner_surf = os.path.join(m2m_folder, "cavities.stl")
	eyes_surf = os.path.join(m2m_folder, "eyes.stl")

	print("Decoupling surfaces")
	print("=================")

	#        decoupling the ventricles does not work, due to a 'bug' in
	#       'decouple_wm_gm_ventricles'. Inside this function, the ventricles
	#       are re-created as binary STL files, which MeshFix, repsonsible for
	#       the decoupling, cannot read. MeshFix returns an error code > 0
	#       which is interpreted by 'decouple_wm_gm_ventricles' as 'no intersection'.
	#       Therefore, the ventricles are never decoupled from the WM, which, luckily,
	#       only becomes a problem, when the ventricles are smoothed.
	if "ventricles" in surfaces:
		print("> Ventricles")
		decouple_wm_gm_ventricles(wm=wm_surf,
				          gm=gm_surf,
				          ventricles=ventricles_surf)
	else:
		print("> GM-WM")
		decouple_surfaces(surf1=wm_surf,
				  surf2=gm_surf,
				  mode="outer-from-inner",
				  cut_inner=True)

	print("> GM-CSF")
	decouple_surfaces(surf1=gm_surf,
			  surf2=csf_surf,
			  mode="outer-from-inner",
			  cut_inner=True)

	print("> CSF-bone")
	decouple_surfaces(surf1=csf_surf,
			  surf2=bone_surf,
			  mode="outer-from-inner",
			  cut_inner=True)

	#        This does not work either because for the decoupling and
	#       detecting of intersections only the two larges shells of the
	#       surfaces are used (meshFix call: --shells 2). In the case
	#       of the air, which consists of multiple shells, using onl
	#       the largest shell for the intersection test with CSF &
	#       bone is not enough and omits intersections between
	#       CSF/bone and the smaller shells of air.
	print("> air-bone")
	decouple_surfaces(surf1=air_decoupled_surf,
			  surf2=bone_surf,
			  mode="neighbor",
			  cut_inner=True)
	decouple_surfaces(surf1=air_relabelled_surf,
			  surf2=bone_surf,
			  mode="neighbor",
			  cut_inner=True)
	decouple_surfaces(surf1=air_inner_surf,
			  surf2=bone_surf,
			  mode="inner-from-outer",
			  cut_inner=True)

	print("> air-csf")
	decouple_surfaces(surf1=air_inner_surf,
			  surf2=csf_surf,
			  mode="neighbor",
			  cut_inner=True)

	print("> eyes-bone")
	decouple_surfaces(surf1=eyes_surf,
			  surf2=bone_surf,
			  mode="neighbor",
			  cut_inner=True)

	print("> bone-skin")
	decouple_surfaces(surf1=bone_surf,
			  surf2=skin_surf,
			  mode="outer-from-inner",
			  cut_inner=True)

	# run gmsh
	#############################################################################################################
	print("Running gmsh")
	print("============")
	fn_msh = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + ".msh")
	fn_msh_gmsh = os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], subject.id + "_gmsh.msh")
	os.popen("gmsh -3 -bin -o " + fn_msh_gmsh + " " + fn_geo_refined).read()
	print("Finished meshing with gmsh ...")

	# apply some transformations from simnibs (this is copied from simnibs/msh/headreco.py #640-785)
	#############################################################################################################
	# load volume mesh
	# using buffered reading due to ineffcient version 2 format written by gmsh
	vmsh = mesh_io.read_msh(fn_msh_gmsh, buffered=True)

	# if ventricles exist, relabel to CSF
	if any(["ventricles" in s for s in surfaces]):
		print("Relabelling ventricles to CSF")
		vmsh = hmu.relabel_compartments(vmsh, [9, 1009], [3, 1003])

	relabel = sorted(glob(os.path.join(m2m_folder, "air_outer_relabel*.stl")), key=str.lower)

	if len(relabel) > 0:
		print("Adjusting volume mesh")
		vmsh = hmu.volume_mesh_relabel(vmsh, relabel, 5)

	vmsh.fn = fn_msh
	print("Fix surface labels")
	vmsh.fix_surface_labels()
	print("Fix thin tetrahedra")
	vmsh.fix_thin_tetrahedra()
	print("Fix tetrahedra node ordering")
	vmsh.fix_th_node_ordering()
	print("Fix triangle node ordering")
	vmsh.fix_tr_node_ordering()
	print("Write final mesh")
	mesh_io.write_msh(vmsh, fn_msh)
	os.remove(fn_msh_gmsh)

print("Creating control volume and GM and WM masks")
T1 = nib.load(os.path.join(m2m_folder, "T1fs_conform.nii.gz"))
final_control = os.path.join(m2m_folder, subject.id + "_final_contr.nii.gz")

hmu.vmesh2nifti(vmsh, T1, final_control)

# print("Checking if the meshing went OK")
# if not hmu.check_volumes_meshed(m2m_folder):
#     os.remove(fn_msh)
#     print("")
#     raise RuntimeError(
#         "Gmsh failed meshing one or more volumes. "
#         "Please try running mesh refinement again "
#         "changing the 'element_size' option in subject object "
#         "(eg. 0.5).")

vmsh = vmsh.crop_mesh(elm_type=4)
mask_names = [os.path.join(m2m_folder, "wm_fromMesh.nii.gz"),
              os.path.join(m2m_folder, "gm_fromMesh.nii.gz")]
affine = T1.affine
n_voxels = T1.header['dim'][1:4]

for k in [1, 2]:
    field = np.zeros(vmsh.elm.nr, dtype=np.uint8)
    field[vmsh.elm.tag1 == k] = 1
    ed = mesh_io.ElementData(field)
    ed.mesh = vmsh
    ed.to_nifti(n_voxels, affine, fn=mask_names[k - 1], qform=T1.header.get_qform(),
                method='assign')

fnames_org = [os.path.join(m2m_folder, "T1fs_nu_conform.nii.gz"),
              os.path.join(m2m_folder, "wm_fromMesh.nii.gz"),
              os.path.join(m2m_folder, "gm_fromMesh.nii.gz")]
# "MNI" will be automatically added at the end of the filenames:
fnames_MNI = [os.path.join(m2m_folder, "toMNI", "T1fs_nu_nonlin.nii.gz"),
              os.path.join(m2m_folder, "toMNI", "wm.nii.gz"),
              os.path.join(m2m_folder, "toMNI", "gm.nii.gz")]

for k in [0, 1, 2]:
    transformations.warp_volume(
        fnames_org[k], m2m_folder, fnames_MNI[k],
        transformation_direction='subject2mni', transformation_type='nonl',
        reference=None, mask=None, labels=None, order=3, binary=k > 0)

transformations.warp_volume(
    fnames_org[0], m2m_folder, os.path.join(m2m_folder, "toMNI", "T1fs_nu_12DOF.nii.gz"),
    # "MNI" will be automatically added
    transformation_direction='subject2mni', transformation_type='12dof',
    reference=None, mask=None, labels=None, order=3)

print("Transforming EEG positions")
eeg_positions = os.path.join(m2m_folder, "eeg_positions")
if not os.path.exists(eeg_positions):
    os.mkdir(eeg_positions)
cap_file = os.path.abspath(os.path.realpath(os.path.join(
        SIMNIBSDIR,
        'resources', 'ElectrodeCaps_MNI',
        'EEG10-10_UI_Jurak_2007.csv')))
cap_out = os.path.join(eeg_positions, 'EEG10-10_UI_Jurak_2007.csv')
geo_out = os.path.join(eeg_positions, 'EEG10-10_UI_Jurak_2007.geo')
transformations.warp_coordinates(
    cap_file, m2m_folder,
    transformation_direction='mni2subject',
    out_name=cap_out,
    out_geo=geo_out)

# delete d2c folder
if os.path.exists(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "d2c_" + subject.id)):
    shutil.rmtree(os.path.join(subject.mesh[mesh_idx_out]["mesh_folder"], "d2c_" + subject.id))

# delete .hdf5 and .xdmf file
if os.path.exists(subject.mesh[mesh_idx_out]["fn_mesh_hdf5"]):
    os.remove(subject.mesh[mesh_idx_out]["fn_mesh_hdf5"])

if os.path.exists(os.path.splitext(subject.mesh[mesh_idx_out]["fn_mesh_hdf5"])[0] + ".xdmf"):
    os.remove(os.path.splitext(subject.mesh[mesh_idx_out]["fn_mesh_hdf5"])[0] + ".xdmf")

# run make_msh_from_mri.py to convert .msh to .hdf5 and create d2c folder
#############################################################################################################
print("Running make_msh_from_mri.py to create ROIs and .hdf5 mesh file ...")
os.popen(f"{sys.executable} {fn_make_msh_from_mri} "
         f"-s {os.path.join(subject.subject_folder, subject.id + '.hdf5')} "
         f"-m {mesh_idx_out}").read()
