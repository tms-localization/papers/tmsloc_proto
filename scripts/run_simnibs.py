#!/usr/bin/env python
"""
This uses the SimNIBS TMS optimize methods to calculate many electric field distributions.
"""
from functools import partial
import argparse
import h5py
import os
import datetime
import numpy as np
from scipy.io import loadmat
from simnibs.simulation import fem, sim_struct
from simnibs import opt_struct, mesh_io
from simnibs import __version__ as v_simnibs
from distutils.version import LooseVersion
import gc

parser = argparse.ArgumentParser(description='Calculate many E-fields and save midlayer E in ROI in .hdf5 file')

# required arguments
parser.add_argument('--folder', help='Folder with stimsites and FEM options. Also output folder.',
                    required=True, type=str)
parser.add_argument('-s', '--fn_subject', help='Subject .hdf5 file',
                    required=True, type=str)

# optional arguments
parser.add_argument('-m', '--mesh_idx', help='Mesh ID', default=0, type=str)
parser.add_argument('-r', '--roi_idx', help='ROI ID', default=1, type=str)
parser.add_argument('--phi_scaling', help='Phi scaling for different simnibs versions', default=1, type=int)
parser.add_argument('--hdf5_fn', help='Results filename for the E fields', default="e.hdf5", type=str)
parser.add_argument('-c', '--cpus', help='How many cpus to use', type=int, default=4)
parser.add_argument('--solver_opt', help='SimNIBS solver options', default='pardiso', type=str)
parser.add_argument('--fem_conf', help='Filename of FEM configuration', default='FEM_config.mat', type=str)
parser.add_argument('--fn_coilpos', help='Filename of TMS position .hdf5', default='stimsites.hdf5', type=str)
parser.add_argument('-a', '--anisotropy_type', help='Anisotropy type: "vn" or "scalar"', type=str, default="vn")
parser.add_argument('--calc_theta_grad', default=False, action='store_true',
                    help='Have the E-field angle to surface normal (theta) and E-field gradient between'
                         'the gray and white matter calculated. Required for neuronal meanfield based mapping.')
args = parser.parse_args()

# import after setting environment solver options
os.environ['SIMNIBS_SOLVER_OPTIONS'] = args.solver_opt
import pynibs
from pynibs.util.simnibs import calc_e_in_midlayer_roi

# set parameters
# ================================================================================
mesh_idx = args.mesh_idx
roi_idx = args.roi_idx
folder = args.folder
subject = pynibs.load_subject_hdf5(args.fn_subject)
cpus = args.cpus
calc_theta_grad = args.calc_theta_grad
phi_scaling = args.phi_scaling

qois = ['E', 'mag', 'norm', 'tan']  # also change array copying at the very end

# read FEM configuration
# ================================================================================
tms_opt = opt_struct.TMSoptimize()
tms_opt = tms_opt.read_mat_struct(loadmat(os.path.join(folder, args.fem_conf)))

dataset = '/tmp'

# print argument infos
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)
for key in args_dict.keys():
    if key == "solver_opt":
        print(f"{key: >15}: {tms_opt.solver_options}")
    else:
        print(f"{key: >15}: {args_dict[key]}")
print(f"{'fn_coil': >15}: {tms_opt.fnamecoil}")

print("-" * 64)

# read subject mesh and roi information
# ================================================================================
print(f"Reading head model (msh): {subject.mesh[mesh_idx]['fn_mesh_msh']}")
mesh_simn = mesh_io.read_msh(subject.mesh[mesh_idx]['fn_mesh_msh'])

print(f"Reading head model (hdf5): {subject.mesh[mesh_idx]['fn_mesh_hdf5']}")
mesh_pyf = pynibs.load_mesh_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])

print(f"Reading ROI #{roi_idx}: {subject.mesh[mesh_idx]['fn_mesh_hdf5']}")
roi = pynibs.load_roi_surface_obj_from_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])[roi_idx]
fn_hdf5 = os.path.join(folder, args.hdf5_fn)
if os.path.exists(fn_hdf5):
    os.rename(fn_hdf5, f'{os.path.splitext(fn_hdf5)[0]}_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}.hdf5')

fields = 'E'  # this check has to be implemented in fem.py, to return (v, dAdt)

if not os.path.exists(folder):
    os.makedirs(folder)

# prepare conductivity tensors
# ================================================================================
cond = sim_struct.SimuList.cond2elmdata(tms_opt, mesh=mesh_simn)

# read coil positions
# ================================================================================
pos_matrices_fn = os.path.join(folder, args.fn_coilpos)
print(f"Reading coil positions: {pos_matrices_fn}")

with h5py.File(pos_matrices_fn, 'r') as f:
    pos_matrices = f['/matsimnibs'][:]

print(f"Preparing simulations for {pos_matrices.shape[-1]} coil positions...")
pos_matrices = [pos_matrices[:, :, i] for i in range(pos_matrices.shape[-1])]
didt_list = [tms_opt.didt for i in pos_matrices]

# do all the FEMs
# ================================================================================
# calc_e_in_midlayer_roi is called from simnibs for each computed efield.

# We need additional data structures for the computation of the e-field gradient
# between the gray and white matter. To speed things up, we pre-compute certain
# geometric information that do not change between simulations.
layer_gm_wm_info = None
if calc_theta_grad:
    layer_gm_wm_info = pynibs.precompute_geo_info_for_layer_field_interpolation(mesh_simn, roi)

    if layer_gm_wm_info is None:
        print("[ WARN ] No layer information found in mesh file, "
              "but calculation of layer specfic e-field values requested. "
              "Will not compute 'theta' and 'e-field gradient'.")

postpro = partial(
        calc_e_in_midlayer_roi,
        roi=roi,
        mesh=mesh_simn,
        qoi=qois,
        layer_gm_wm_info=layer_gm_wm_info,
)

print("Starting: fem.tms_many_simulations ...")
t = datetime.datetime.now()

fem.tms_many_simulations(
        mesh=mesh_simn,
        cond=cond,
        fn_coil=tms_opt.fnamecoil,
        matsimnibs_list=pos_matrices,
        didt_list=didt_list,
        fn_hdf5=fn_hdf5,
        dataset=dataset,
        post_pro=postpro,
        solver_options=tms_opt.solver_options,
        n_workers=cpus,
        field=fields)

print(f"Done. FEM solving took {datetime.datetime.now() - t}")

# reshape .hdf5 file
# ================================================================================
print(f"Reshaping .hdf5")

layer_data = dict()
with h5py.File(fn_hdf5, 'r') as h5:
    # undo zero padding by storing just as many values as elements on layer
    num_roi_elmts = roi.node_number_list.shape[0]
    E = h5[dataset][:, :num_roi_elmts, 0:3]
    E_mag = h5[dataset][:, :num_roi_elmts, 3]
    E_norm = h5[dataset][:, :num_roi_elmts, 4]
    E_tan = h5[dataset][:, :num_roi_elmts, 5]

    qoi_idx = 6
    if calc_theta_grad:
        for layer in roi.layers:
            # undo zero padding by storing just as many values as elements on layer
            num_layer_elmts = layer.surface.elm.nr
            layer_data[layer.id] = dict()
            layer_data[layer.id]["E_mag"] = h5[dataset][:, :num_layer_elmts, qoi_idx]
            qoi_idx += 1
            layer_data[layer.id]["E_theta"] = h5[dataset][:, :num_layer_elmts, qoi_idx]
            qoi_idx += 1
            layer_data[layer.id]["E_gradient"] = h5[dataset][:, :num_layer_elmts, qoi_idx]
            qoi_idx += 1

with h5py.File(fn_hdf5.replace('.hdf5','reshaped.hdf5'), 'w') as h5:
    h5.create_dataset('/E', data=E)
    h5.create_dataset('/E_mag', data=E_mag)
    h5.create_dataset('/E_norm', data=E_norm)
    h5.create_dataset('/E_tan', data=E_tan)
    if calc_theta_grad:
        for layer in roi.layers:
            h5.create_group(layer.id)
            for qoi, data in layer_data[layer.id].items():
                h5.create_dataset(f'/{layer.id}/{qoi}', data=data)
