#!/usr/bin/env python

"""
Merges experimental data and saves results in fn_exp specified in subject object file.
Also corrects coil <-> head distance in separate file '*_corrected.csv'.

merge_exp_data.py -s <subjectobjfile> -e <exp_id> -m <mesh_idx> -o -d -r

-s <str> ... filename (incl. path) of subject object file
-e <int> ... experiment ID
-m <int> ... mesh idx
-o ... coil position outlier correction (for conditions with multiple same stimulation locations)
-d ... coil <-> head distance correction
-r ... remove coil position with distance > +-5 mm from skin surface
-p ... plot MEPs
--start_mep ... Start of time frame after TMS pulse where p2p value is evaluated (in ms)
--end_mep ... End of time frame after TMS pulse where p2p value is evaluated (in ms)
"""

import os
import h5py
import pynibs
import argparse
import numpy as np
import pandas as pd

# Read input
parser = argparse.ArgumentParser(description='Run SimNIBS to determine electric fields\n')
parser.add_argument('-s', '--fn_subject',
                    help='Filename (incl. path) of subject object file', required=True, type=str)
parser.add_argument('-e', '--exp_idx',
                    help='Experiment ID', required=True, type=str)
parser.add_argument('-m', '--mesh_idx',
                    help='Mesh idx', required=True)
parser.add_argument('-o', '--outlier_correction',
                    help='Remove outliers of coil position.', required=False,
                    action='store_true')
parser.add_argument('-d', '--distance_correction',
                    help='Perform coil <-> head distance correction', required=False,
                    action='store_true')
parser.add_argument('-r', '--remove_coil_skin_distance_outlier',
                    help='Remove coil position with distance > -5/+2 mm from skin surface', required=False,
                    action='store_true')
parser.add_argument('-p', '--plot',
                    help='Plot MEPs', required=False,
                    action='store_true')
parser.add_argument('--start_mep',
                    help='Start of time frame after TMS pulse where p2p value is evaluated (in ms)', required=False,
                    default=18)
parser.add_argument('--end_mep',
                    help='End of time frame after TMS pulse where p2p value is evaluated (in ms)', required=False,
                    default=35)

args = parser.parse_args()

fn_subject = os.path.abspath(args.fn_subject)
exp_id = args.exp_idx
mesh_idx = args.mesh_idx
start_mep = float(args.start_mep)
end_mep = float(args.end_mep)

if args.outlier_correction:
    coil_outlier_corr = True
else:
    coil_outlier_corr = False

if args.distance_correction:
    coil_distance_corr = True
else:
    coil_distance_corr = False

if args.remove_coil_skin_distance_outlier:
    remove_coil_skin_distance_outlier = True
else:
    remove_coil_skin_distance_outlier = False

if args.plot:
    plot = True
else:
    plot = False

# load subject object
subject = pynibs.load_subject(fn_subject)
drop_mep_idx = None
mep_onsets = None
cfs_data_column = 0
channels = ["channel_0"]

if 'mep_onsets' in subject.exp[exp_id].keys():
    mep_onsets = subject.exp[exp_id]['mep_onsets']

if 'drop_mep_idx' in subject.exp[exp_id].keys():
    drop_mep_idx = subject.exp[exp_id]['drop_mep_idx']

if "cfs_data_column" in subject.exp[exp_id].keys():
    cfs_data_column = subject.exp[exp_id]["cfs_data_column"]
    channels = ["channel_" + str(i) for i in range(len(cfs_data_column))]

if "channels" in subject.exp[exp_id].keys():
    channels = subject.exp[exp_id]["channels"]
    cfs_data_column = range(len(channels))

if "fn_exp_hdf5" in subject.exp[exp_id].keys():
    if os.path.exists(subject.exp[exp_id]["fn_exp_hdf5"][0]):
        os.remove(subject.exp[exp_id]["fn_exp_hdf5"][0])

if "fn_exp_csv" in subject.exp[exp_id].keys():
    if os.path.exists(subject.exp[exp_id]["fn_exp_csv"][0]):
        os.remove(subject.exp[exp_id]["fn_exp_csv"][0])

# merged data will be saved in mesh specific subfolder
subject.exp[exp_id]["fn_exp_hdf5"] = [os.path.join(subject.subject_folder, "exp", exp_id, f"mesh_{mesh_idx}", "experiment.hdf5")]

if not os.path.exists(os.path.split(subject.exp[exp_id]["fn_exp_hdf5"][0])[0]):
    os.makedirs(os.path.split(subject.exp[exp_id]["fn_exp_hdf5"][0])[0])

# merge experimental data to experiment.hdf5
verbose = True
if subject.exp[exp_id]['nnav_system'].lower() == "localite":

    # this is the csv version
    # pynibs.merge_exp_data_localite(subject=subject, exp_id=exp_id, mesh_idx=mesh_idx,
    #                                 coil_outlier_corr=coil_outlier_corr,
    #                                 verbose=verbose, version='csv')

    # this is the hdf5 version
    pynibs.expio.localite.merge_exp_data_localite(subject=subject,
                                                  exp_idx=exp_id,
                                                  mesh_idx=mesh_idx,
                                                  coil_outlier_corr_cond=coil_outlier_corr,
                                                  remove_coil_skin_distance_outlier=remove_coil_skin_distance_outlier,
                                                  coil_distance_corr=coil_distance_corr,
                                                  verbose=verbose,
                                                  drop_mep_idx=drop_mep_idx,
                                                  mep_onsets=mep_onsets,
                                                  channels=channels,
                                                  cfs_data_column=cfs_data_column,
                                                  plot=plot,
                                                  start_mep=start_mep,
                                                  end_mep=end_mep)

elif subject.exp[exp_id]['nnav_system'].lower() == "visor":
    pynibs.expio.visor.merge_exp_data_visor(subject=subject,
                                            exp_id=exp_id,
                                            mesh_idx=mesh_idx,
                                            verbose=verbose)


elif subject.exp[exp_id]['nnav_system'].lower() == "brainsight":
    pynibs.expio.brainsight.merge_exp_data_brainsight(subject=subject,
                                                      exp_idx=exp_id,
                                                      mesh_idx=mesh_idx,
                                                      coil_outlier_corr_cond=coil_outlier_corr,
                                                      remove_coil_skin_distance_outlier=remove_coil_skin_distance_outlier,
                                                      coil_distance_corr=coil_distance_corr,
                                                      verbose=verbose,
                                                      plot=plot,
                                                      start_mep=start_mep,
                                                      end_mep=end_mep)
else:
    raise NotImplementedError(f"Neuronavigation system {subject.exp[exp_id]['nnav_system']} not supported!"
                        f"Use either 'Localite' or 'Visor'...")

# Plot coil positions with MEP data
#######################################################################################################################
if 'plot_coil_ps_fn' in subject.exp[exp_id].keys():
    fn_coil_pos = subject.exp[exp_id]['plot_coil_ps_fn']
else:
    fn_coil_pos = "plot_coil_pos.hdf5"
fn_coil_pos = os.path.join(os.path.split(subject.exp[exp_id]["fn_exp_hdf5"][0])[0], fn_coil_pos)
df_exp = pd.read_hdf(subject.exp[exp_id]["fn_exp_hdf5"][0], "phys_data/postproc/EMG")

mep = dict()
for channel in subject.exp[exp_id]["channels"]:
    mep[channel] = np.array(df_exp[f"p2p_{channel}"])

# plot coil positions
pynibs.create_stimsite_from_exp_hdf5(fn_exp=subject.exp[exp_id]["fn_exp_hdf5"][0],
                                     fn_hdf=fn_coil_pos,
                                     datanames=subject.exp[exp_id]["channels"],
                                     data=np.vstack([mep[channel] for channel in mep.keys()]).T,
                                     overwrite=True)

# Create matsimnibs.hdf5
#######################################################################################################################
if 'fn_matsimnibs' in subject.exp[exp_id].keys():
    fn_matsimnibs = subject.exp[exp_id]['fn_matsimnibs']
else:
    fn_matsimnibs = "matsimnibs.hdf5"
fn_matsimnibs = os.path.join(os.path.split(subject.exp[exp_id]["fn_exp_hdf5"][0])[0], fn_matsimnibs)
fn_exp_hdf5 = subject.exp[exp_id]["fn_exp_hdf5"][0]

print(f"Reading: {fn_exp_hdf5}")
df_stim_data = pd.read_hdf(fn_exp_hdf5, "stim_data")

matsimnibs = np.zeros((4, 4, df_stim_data.shape[0]))

for i in range(df_stim_data.shape[0]):
    matsimnibs[:, :, i] = df_stim_data["coil_mean"].iloc[i]

print(f"Writing: {fn_matsimnibs}")
with h5py.File(fn_matsimnibs, "w") as f:
    f.create_dataset("matsimnibs", data=matsimnibs)
