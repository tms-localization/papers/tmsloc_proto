#!/usr/bin/env python
"""
This uses the SimNIBS TMS optimize methods to calculate many electric field distributions.
"""
import os
import time
import h5py
import pynibs
import argparse
import datetime
import numpy as np
import pandas as pd
from scipy.io import loadmat
from simnibs.simulation.onlinefem import OnlineFEM
from simnibs import opt_struct, mesh_io, RegionOfInterest
from collections import deque

parser = argparse.ArgumentParser(description='Calculate many E-fields and save midlayer E in ROI in .hdf5 file')

# required arguments
parser.add_argument('--folder', help='Folder with stimsites and FEM options. Also output folder.',
                    required=True, type=str)
parser.add_argument('-s', '--fn_subject', help='Subject .hdf5 file',
                    required=True, type=str)

# optional arguments
parser.add_argument('-m', '--mesh_idx', help='Mesh ID', default=0, type=str)
parser.add_argument('-r', '--roi_idx', help='ROI ID', default=1, type=str)
parser.add_argument('--hdf5_fn', help='Results filename for the E fields', default="e.hdf5", type=str)
parser.add_argument('--solver_opt', help='SimNIBS solver options', default='pardiso', type=str)
parser.add_argument('--fn_coilpos', help='Filename of TMS position .hdf5', default='stimsites.hdf5', type=str)
parser.add_argument('--fn_coil', help='Filename of TMS coil', type=str)
parser.add_argument('-a', '--anisotropy_type', help='Anisotropy type: "vn" or "scalar"', type=str, default="vn")
parser.add_argument('--calc_theta_grad', default=False, action='store_true',
                    help='Have the E-field angle to surface normal (theta) and E-field gradient between'
                         'the gray and white matter calculated. Required for neuronal meanfield based mapping.')
parser.add_argument('-e', '--fn_exp', help='Filename of experiment.hdf5', type=str, default=False)
args = parser.parse_args()

# import after setting environment solver options
os.environ['SIMNIBS_SOLVER_OPTIONS'] = args.solver_opt

# set parameters
# ================================================================================
mesh_idx = args.mesh_idx
roi_idx = args.roi_idx
folder = args.folder
subject = pynibs.load_subject_hdf5(args.fn_subject)
calc_theta_grad = args.calc_theta_grad
fn_exp = args.fn_exp
fn_coil = args.fn_coil
anisotropy_type = args.anisotropy_type
solver_options = args.solver_opt

# read FEM configuration
# ================================================================================
dataset = '/tmp'

# print argument infos
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)
for key in args_dict.keys():
    if key == "solver_opt":
        print(f"{key: >15}: {solver_options}")
    else:
        print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

# read subject mesh and roi information
# ================================================================================
print(f"Reading head model (msh): {subject.mesh[mesh_idx]['fn_mesh_msh']}")
mesh_simn = mesh_io.read_msh(subject.mesh[mesh_idx]['fn_mesh_msh'])

print(f"Reading ROI #{roi_idx}: {subject.mesh[mesh_idx]['fn_mesh_hdf5']}")
roi_pynibs = pynibs.load_roi_surface_obj_from_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])[roi_idx]
fn_hdf5 = os.path.join(folder, args.hdf5_fn)
if os.path.exists(fn_hdf5):
    os.rename(fn_hdf5, f'{os.path.splitext(fn_hdf5)[0]}_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}.hdf5')

# create SimNIBS ROIs (sF matrix etc. for fast interpolation)
# ================================================================================
print(f"Initializing SimNIBS ROI: {roi_idx}")
roi_list_simnibs = [RegionOfInterest(center=roi_pynibs.tri_center_coord_mid, mesh=mesh_simn)]
# determine surface normals in elements
# p1_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 0] - 1, :]
# p2_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 1] - 1, :]
# p3_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 2] - 1, :]
p1_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 0] - np.min(roi_pynibs.node_number_list[:, :3]), :]
p2_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 1] - np.min(roi_pynibs.node_number_list[:, :3]), :]
p3_tri = roi_pynibs.node_coord_mid[roi_pynibs.node_number_list[:, 2] - np.min(roi_pynibs.node_number_list[:, :3]), :]

triangles_normals = np.cross(p2_tri - p1_tri, p3_tri - p1_tri).T
triangles_normals /= np.linalg.norm(triangles_normals, axis=0)
triangles_normals = triangles_normals.T

roi_list_simnibs[-1].triangles_normals = triangles_normals
roi_list_simnibs[-1].lower_upper_distance = 0.1

roi_list_simnibs_lower = [RegionOfInterest(
    center=roi_pynibs.tri_center_coord_mid - roi_list_simnibs[-1].lower_upper_distance / 2 * triangles_normals,
    mesh=mesh_simn)]
roi_list_simnibs_upper = [RegionOfInterest(
    center=roi_pynibs.tri_center_coord_mid + roi_list_simnibs[-1].lower_upper_distance / 2 * triangles_normals,
    mesh=mesh_simn)]

n_roi = 1

if roi_pynibs.layer:
    layer_gm_wm_info = pynibs.precompute_geo_info_for_layer_field_interpolation(mesh_simn, roi_pynibs)
    for i_layer, layer in enumerate(roi_pynibs.layers):
        print(f"Initializing SimNIBS ROI: {roi_idx} layer {layer.id}")
        # tri_center_layer = np.mean(layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, :3] - 1],
        #                            axis=1)
        tri_center_layer = np.mean(layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, :3] - np.min(layer.surface.elm.node_number_list[:, :3])],
                                   axis=1)
        roi_list_simnibs.append(RegionOfInterest(center=tri_center_layer, mesh=mesh_simn))

        # determine surface normals in elements
        p1_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 0] - np.min(layer.surface.elm.node_number_list[:, :3]), :]
        p2_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 1] - np.min(layer.surface.elm.node_number_list[:, :3]), :]
        p3_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 2] - np.min(layer.surface.elm.node_number_list[:, :3]), :]

        # p1_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 0], :]
        # p2_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 1], :]
        # p3_tri = layer.surface.nodes.node_coord[layer.surface.elm.node_number_list[:, 2], :]

        triangles_normals = np.cross(p2_tri - p1_tri, p3_tri - p1_tri).T
        triangles_normals /= np.linalg.norm(triangles_normals, axis=0)
        triangles_normals = triangles_normals.T  # nelm * 3

        # add normals to object
        roi_list_simnibs[-1].triangles_normals = triangles_normals

        # add distance between lower and upper
        roi_list_simnibs[-1].lower_upper_distance = layer_gm_wm_info[layer.id]["layer_gm_dist"] + \
                                                    layer_gm_wm_info[layer.id]["layer_wm_dist"]

        # create ROIs for lower and upper interpolation
        roi_list_simnibs_lower.append(
            RegionOfInterest(center=layer_gm_wm_info[layer.id]["assoc_wm_points"], mesh=mesh_simn))
        roi_list_simnibs_upper.append(
            RegionOfInterest(center=layer_gm_wm_info[layer.id]["assoc_gm_points"], mesh=mesh_simn))

        n_roi += 1

roi_list_simnibs_all = roi_list_simnibs + roi_list_simnibs_lower + roi_list_simnibs_upper

if not os.path.exists(folder):
    os.makedirs(folder)

# read coil positions
# ================================================================================
pos_matrices_fn = os.path.join(folder, args.fn_coilpos)
print(f"Reading coil positions: {pos_matrices_fn}")

with h5py.File(pos_matrices_fn, 'r') as f:
    matsimnibs = f['/matsimnibs'][:]
n_pos = matsimnibs.shape[-1]

# read coil current and scale from A/us to A/s for SimNIBS
if fn_exp:
    df_stim_data = pd.read_hdf(fn_exp, "stim_data")
    didt = np.array(df_stim_data["current"]) * 1e6
else:
    didt = np.ones(n_pos) * 1e6

# prepare FEM
# ================================================================================
print(f"Preparing FEM.")
ofem = OnlineFEM(mesh=mesh_simn,
                 method="TMS",
                 fn_coil=fn_coil,
                 roi=roi_list_simnibs_all,
                 anisotropy_type=anisotropy_type,
                 solver_options=solver_options,
                 fn_results=None,
                 useElements=True,
                 dataType=[1] * len(roi_list_simnibs_all))  # Changed by Ying

# run all FEMs
# ================================================================================
# initialize arrays containing fields in ROIs
E = []
E_mag = []
E_norm = []
E_tan = []
E_gradient = []
E_theta = []

for roi in roi_list_simnibs:
    n_tri = roi.center.shape[0]  # Ying point->center
    E.append(np.zeros((n_pos, n_tri, 3)))
    E_mag.append(np.zeros((n_pos, n_tri)))
    E_norm.append(np.zeros((n_pos, n_tri)))
    E_tan.append(np.zeros((n_pos, n_tri)))
    E_gradient.append(np.zeros((n_pos, n_tri)))
    E_theta.append(np.zeros((n_pos, n_tri)))

print("Starting FEM ...")
t_start_total = time.time()
for i_pos in range(n_pos):
    t_start_run = time.time()

    e = ofem.update_field(matsimnibs=matsimnibs[:, :, i_pos], sim_idx_hdf5=0, didt=didt[i_pos])

    # determine e-field quantities
    for i_roi, roi in enumerate(roi_list_simnibs):
        E[i_roi][i_pos, :, :] = e[0][i_roi]  # n_ele * 3
        E_mag[i_roi][i_pos, :] = np.linalg.norm(e[0][i_roi], axis=1)  # n_ele * 1
        E_norm[i_roi][i_pos, :] = -np.sum(e[0][i_roi] * roi.triangles_normals, axis=1)
        E_tan[i_roi][i_pos, :] = np.sqrt(E_mag[i_roi][i_pos, :] ** 2 - E_norm[i_roi][i_pos, :] ** 2)
        E_theta[i_roi][i_pos, :] = np.arccos(np.sum(e[0][i_roi] / np.linalg.norm(e[0][i_roi], axis=1)[:, np.newaxis] *
                                                    roi.triangles_normals, axis=1)) / np.pi * 180  # plot it
        E_gradient[i_roi][i_pos, :] = (np.linalg.norm(e[0][i_roi + 2 * n_roi], axis=1) -
                                       np.linalg.norm(e[0][i_roi + n_roi], axis=1)) / \
                                      roi.lower_upper_distance / \
                                      E_mag[i_roi][i_pos, :] \
                                      * 100  # plot it

    print(f"Finished simulation {i_pos + 1}/{n_pos} ({(time.time() - t_start_run):.3f}s)")

print(f"Done. FEM solving took {time.time() - t_start_total}")

# saving results
# ================================================================================
with h5py.File(fn_hdf5, 'w') as h5:
    for i_roi in range(len(roi_list_simnibs)):
        if i_roi == 0:
            suffix = ""
        else:
            suffix = roi_pynibs.layers[i_roi - 1].id

        h5.create_dataset(f"{suffix}/E", data=E[i_roi])
        h5.create_dataset(f"{suffix}/E_mag", data=E_mag[i_roi])
        h5.create_dataset(f"{suffix}/E_norm", data=E_norm[i_roi])
        h5.create_dataset(f"{suffix}/E_tan", data=E_tan[i_roi])
        h5.create_dataset(f"{suffix}/E_gradient", data=E_gradient[i_roi])
        h5.create_dataset(f"{suffix}/E_theta", data=E_theta[i_roi])
