#!/usr/bin/env python

"""
This script calculates the goodness-of-fits for each muscle in the ROI.
"""

import os
import h5py
import pynibs
import argparse
import pandas as pd
import numpy as np

from pynibs.regression import regress_data
from pynibs.neuron.neuron_regression import calc_e_effective

parser = argparse.ArgumentParser(description='Calculate fits for 3 muscles in ROI using all TMS pulses '
                                             'and save results in .hdf5 file\n')

# required arguments
parser.add_argument('-s', '--fn_subject', help='Path to subject obj',
                    required=True, type=str)
parser.add_argument('-m', '--mesh_idx', help='Mesh idx',
                    required=True, type=str)
parser.add_argument('-e', '--exp_idx', help='Exp idx',
                    required=True, type=str)
parser.add_argument('-r', '--roi_idx', help='ROI idx',
                    required=False, type=str, default=1)
parser.add_argument('-n', '--n_cpu', help='How many cpus to use',
                    type=int, default=10)
parser.add_argument('-f', '--function', help='Function to fit data to ("linear", "sigmoid4", "sigmoid4_log")',
                    type=str, default='sigmoid4')
parser.add_argument('-i', '--n_refit', help='Number of refit iterations',
                    required=False, type=int, default=20)
parser.add_argument('-v', '--verbose', help='Show detailed output messages',
                    required=False, action='store_true')
parser.add_argument('--map2surf', help='Map results to whole brain surface',
                    required=False, action='store_true')
parser.add_argument('--convergence', help='Assess convergence metrics (n vs n-1)',
                    required=False, action='store_true', default=False)
parser.add_argument('--layerid', help='Perform mapping based on neuronal meanfield model on the specified layer. ("L23", "L5")',
                    required=False)
parser.add_argument('--neuronmodel', help='Select neuron model for localization. '
                                          '("sensitivity_weighting", "threshold_subtract", "threshold_binary",'
                                          ' "IOcurve")',
                    required=False, default=None)
parser.add_argument('--waveform', help='Waveform of TMS pulse. ("monophasic", "biphasic")',
                    required=False, default='biphasic')

args = parser.parse_args()

# print parameter info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)
print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

# parameters
# ================================================================================
# file containing the subject information
fn_subject = args.fn_subject

# load subject object
subject = pynibs.load_subject(fn_subject)

# exp identifier
exp_idx = args.exp_idx

# mesh identifier
mesh_idx = args.mesh_idx

# ROI index the fields are extracted for goodness-of-fit calculation
roi_idx = args.roi_idx

# number threads (cpu cores) to use for parallelization
n_cpu = args.n_cpu

# show detailed output messages
verbose = args.verbose

# functions to perform the fit with
functions = [getattr(pynibs, args.function)]

# hand muscles
muscles = subject.exp[exp_idx]["channels"]

# quantities of electric field the fit is performed for
e_qoi_list = ["E_mag"]

# map data to whole brain surface
map2surf = args.map2surf

# perform mapping using neuronal meanfield model
layerid = args.layerid

# neuron regression approach
neuronmodel = args.neuronmodel

# TMS waveform
waveform = args.waveform

# number of refit iterations
n_refit = args.n_refit

# output measure ("SR": Relative standard error of regression, "R2": R2 score)
score_type = "R2"

# Assess the convergence by comparing n vs n-1 stimulations
convergence = args.convergence
norm_to_perc = 99

# loading subject objects
subject = pynibs.load_subject(fname=fn_subject)
subject_dir = os.path.split(fn_subject)[0]

# parent folder containing the output data
fn_results = os.path.join(subject_dir, "results", f"exp_{exp_idx}", "r2",
                          f"mesh_{mesh_idx}", f"roi_{roi_idx}")

# file containing the electric field in the ROI [n_zaps x n_ele]
fn_e_roi = os.path.join(subject_dir, "results", f"exp_{exp_idx}", "electric_field",
                        f"mesh_{mesh_idx}", f"roi_{roi_idx}", f"e_scaled.hdf5")

if not os.path.exists(fn_e_roi):
    raise FileNotFoundError(f"No electric field file found!")

fn_result_suffix = f"_neuron_{layerid}_{neuronmodel}" if layerid is not None else ""

subject.exp[exp_idx]["fn_exp_hdf5"] = [os.path.join(subject.subject_folder, "exp", exp_idx, f"mesh_{mesh_idx}",
                                                    "experiment.hdf5")]

print(f"Processing subject: {subject.id}, experiment: {exp_idx}")
print("=====================================================")

# load ROI
roi = pynibs.load_roi_surface_obj_from_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])[roi_idx]
con = roi.node_number_list
mesh_folder = subject.mesh[mesh_idx]["mesh_folder"]

# load exp data
df_phys_data_postproc_EMG = pd.read_hdf(subject.exp[exp_idx]["fn_exp_hdf5"][0], "phys_data/postproc/EMG")

convergence_results = {'muscle': [],
                       'qoi': [],
                       'fun': [],
                       'nrmsd': [],
                       'geodesic dist': []}

for i_m, m in enumerate(muscles):
    try:
        m = str(int(m))
    except ValueError:
        pass
    print(f"\n> Muscle: {m}")

    # load MEPs
    mep = np.array(df_phys_data_postproc_EMG[f"p2p_{m}"])

    for i_fun, fun in enumerate(functions):
        if neuronmodel and not (fun == pynibs.sigmoid4 or fun == pynibs.sigmoid4_log):
            raise NotImplementedError("Neuron regression can only be "
                                      "performed with sigmoid4 or sigmoid4_log functions.")

        print(f"> fun: {fun.__name__}")

        gof_dict = dict()

        for i_e_qoi, e_qoi in enumerate(e_qoi_list):
            print(f"> E-QOI: {e_qoi}")

            if e_qoi == "E_norm":
                select_signed_data = True
            else:
                select_signed_data = False

            # load electric field
            with h5py.File(fn_e_roi, "r") as f:
                if layerid is not None:
                    print(f"Performing neuron-enhanced mapping on Layer {layerid} using '{neuronmodel}' approach")
                    e_matrix = f[f"/{layerid}/E_mag"][:]
                    e_matrix_orig = f[f"/{layerid}/E_mag"][:]
                    # load e-field angle to local surface normal (= theta)
                    # and relative gradient of e-field between GM and WM (= gradient)
                    theta = f[f"/{layerid}/E_theta"][:]
                    gradient = f[f"/{layerid}/E_gradient"][:]

                    # calculate by how much the individual threshold of the
                    # neuronal population was exceeded by the applied e-field
                    # neuronmodel can be either "threshold" or "IOcurve"
                    e_matrix = calc_e_effective(e=e_matrix,
                                                layerid=layerid,
                                                theta=theta,
                                                gradient=gradient,
                                                neuronmodel=neuronmodel,
                                                mep=mep,
                                                waveform=waveform)

                    if layerid is not None:
                        for layer in roi.layers:
                            if layer.id == layerid:
                                nodes = layer.surface.nodes.node_coord
                                con = layer.surface.elm.node_number_list[:, :3] - np.min(layer.surface.elm.node_number_list[:, :3])
                else:
                    e_matrix = f[e_qoi][:]
                    nodes = roi.node_coord_mid
                    con = roi.node_number_list

            # check for zero e-fields and filter them (problems in FEM!)
            zero_mask = (e_matrix == 0).all(axis=1)

            if zero_mask.any():
                print(f"Warning! {np.sum(zero_mask)} zero e-fields detected in element! Check FEM! Ignoring them for now!")
                e_matrix = e_matrix[np.logical_not(zero_mask), :]
                mep = mep[np.logical_not(zero_mask)]

            if layerid is not None and neuronmodel == "IOcurve":
                gof_dict[e_qoi] = 1/np.var(e_matrix, axis=0)

            else:
                gof_dict[e_qoi] = regress_data(e_matrix=e_matrix,
                                               mep=mep,
                                               fun=fun,
                                               n_cpu=n_cpu,
                                               con=con,
                                               n_refit=n_refit,
                                               return_fits=False,
                                               score_type=score_type,
                                               verbose=verbose,
                                               pool=None,
                                               refit_discontinuities=True,
                                               select_signed_data=select_signed_data)

            if convergence:
                print(f"Computing n-1 results to assess convergence.")
                res_prev = regress_data(e_matrix=e_matrix[:-1, :],
                                        mep=mep[:-1],
                                        fun=fun,
                                        n_cpu=n_cpu,
                                        con=con,
                                        n_refit=n_refit,
                                        return_fits=False,
                                        score_type=score_type,
                                        verbose=verbose,
                                        pool=None,
                                        refit_discontinuities=True,
                                        select_signed_data=select_signed_data)

                # compute normalised root-mean-square deviation between n and n-1 solution
                norm_to_perc = 99
                res_final = gof_dict[e_qoi] / np.percentile(gof_dict[e_qoi][gof_dict[e_qoi] > 0],
                                                            norm_to_perc)
                res_prev = res_prev / np.percentile(res_prev[res_prev > 0], norm_to_perc)
                nrmsd = pynibs.nrmsd(res_final, res_prev)

                # compute geodesic distance between best element n and n-1 solution
                best_elm = np.argmax(res_final)
                best_elm_prev = np.argmax(res_prev)
                nodes_dist, tris_dist = pynibs.geodesic_dist(nodes,
                                                             con,
                                                             best_elm,
                                                             source_is_node=False)
                geod_dist = tris_dist[best_elm_prev]

                convergence_results['muscle'].append(m)
                convergence_results['qoi'].append(e_qoi)
                convergence_results['fun'].append(fun.__name__)
                convergence_results['nrmsd'].append(nrmsd)
                convergence_results['geodesic dist'].append(geod_dist)

        # save results
        fn_gof_hdf5 = os.path.join(fn_results, m, fun.__name__, f"r2{fn_result_suffix}_roi_data.hdf5")

        # create folder
        if not os.path.exists(os.path.split(fn_gof_hdf5)[0]):
            os.makedirs(os.path.split(fn_gof_hdf5)[0])
        fn_geo_hdf5 = os.path.join(os.path.split(fn_gof_hdf5)[0], f"r2{fn_result_suffix}_roi_geo.hdf5")

        # write hdf5 _geo file
        pynibs.write_geo_hdf5_surf(out_fn=fn_geo_hdf5,
                                   points=nodes,
                                   con=con,
                                   replace=True,
                                   hdf5_path='/mesh')

        # write _data file
        data = [gof_dict[e_qoi] for e_qoi in e_qoi_list]
        data_names = [f'c_{e_qoi}' for e_qoi in e_qoi_list]

        print(f"Writing results to {fn_gof_hdf5}.")
        pynibs.write_data_hdf5_surf(data=data,
                                    data_names=data_names,
                                    data_hdf_fn_out=fn_gof_hdf5,
                                    geo_hdf_fn=fn_geo_hdf5,
                                    replace=True)

        if not map2surf or layerid is not None: # skip this step also when mapping on the layers is requested
            continue
        print("> Mapping data to brain surface")
        c_mapped = pynibs.map_data_to_surface(datasets=data,
                                              points_datasets=[roi.node_coord_mid] * len(data),
                                              con_datasets=[roi.node_number_list] * len(data),
                                              fname_fsl_gm=[None, None],
                                              fname_fsl_wm=[None, None],
                                              fname_midlayer=[
                                                  os.path.join(mesh_folder, subject.mesh[mesh_idx]['fn_lh_midlayer']),
                                                  os.path.join(mesh_folder, subject.mesh[mesh_idx]['fn_rh_midlayer'])
                                              ],
                                              delta=subject.roi[mesh_idx][roi_idx]['delta'],
                                              input_data_in_center=True,
                                              return_data_in_center=True,
                                              data_substitute=-1)

        # recreate complete midlayer surface to write in .hdf5 geo file
        points_midlayer, con_midlayer = pynibs.make_GM_WM_surface(gm_surf_fname=[subject.mesh[mesh_idx]['fn_lh_gm'],
                                                                                 subject.mesh[mesh_idx]['fn_rh_gm']],
                                                                  wm_surf_fname=[subject.mesh[mesh_idx]['fn_lh_wm'],
                                                                                 subject.mesh[mesh_idx]['fn_rh_wm']],
                                                                  mesh_folder=mesh_folder,
                                                                  midlayer_surf_fname=[
                                                                      subject.mesh[mesh_idx]['fn_lh_midlayer'],
                                                                      subject.mesh[mesh_idx]['fn_rh_midlayer']
                                                                  ],
                                                                  delta=subject.roi[mesh_idx][roi_idx]['delta'],
                                                                  X_ROI=None,
                                                                  Y_ROI=None,
                                                                  Z_ROI=None,
                                                                  layer=1,
                                                                  fn_mask=None)

        # save hdf5 _geo file (mapped)
        print(f" > Writing mapped brain and roi to {os.path.join(os.path.split(fn_gof_hdf5)[0], f'r2{fn_result_suffix}_data.hdf5')}")
        pynibs.write_geo_hdf5_surf(out_fn=os.path.join(os.path.split(fn_gof_hdf5)[0], f"r2{fn_result_suffix}_geo.hdf5"),
                                   points=points_midlayer,
                                   con=con_midlayer,
                                   replace=True,
                                   hdf5_path='/mesh')

        # save hdf5 _data file (mapped)

        pynibs.write_data_hdf5_surf(data=c_mapped,
                                    data_names=data_names,
                                    data_hdf_fn_out=os.path.join(os.path.split(fn_gof_hdf5)[0], f"r2{fn_result_suffix}_data.hdf5"),
                                    geo_hdf_fn=os.path.join(os.path.split(fn_gof_hdf5)[0], f"r2{fn_result_suffix}_geo.hdf5"),
                                    replace=True)

print("DONE")

if convergence:
    print("Convergence for n vs n-1 stimulations:")
    convergence_results = pd.DataFrame().from_dict(convergence_results)
    print(convergence_results)
    convergence_fn = os.path.join(fn_results, f"convergence{fn_result_suffix}.csv")
    print(f"Saved to {convergence_fn}")
    convergence_results.to_csv(convergence_fn)
print("====")
