#!/usr/bin/env python

"""
Determine optimal coil position/orientation from localization experiment.
"""

import os
import re
import h5py
import pynibs
import simnibs
import warnings
import argparse
import numpy as np
from simnibs import opt_struct, mesh_io

if int(simnibs.__version__[0]) < 4 or simnibs.__version__ == '4.0b1':
    from simnibs.utils.nnav import write_tms_navigator_im
else:
    from simnibs.utils.nnav import localite

    loc = localite()
    write_tms_navigator_im = loc.write

# set up command line argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--fn_subject', help='Path to *.hdf5-file of subject', required=True)
parser.add_argument('-m', '--mesh_idx', help='Mesh ID', required=True, type=str)
parser.add_argument('-e', '--exp_idx', help='Experiment ID', required=True, type=str)
parser.add_argument('-n', '--n_cpu', help='How many cpus to use', type=int, default=4)
parser.add_argument('-a', '--anisotropy_type', help='Anisotropy "vn" or "scalar"', type=str, default="vn")
parser.add_argument('-p', '--pardiso', action='store_true', help="Use pardiso solver")
parser.add_argument('-t', '--target', nargs='+', required=True, help='R2 results file or x/y/z coordinates')
parser.add_argument('-q', '--qoi', help='Electric field component ("E_mag", "E_norm", "E_tan")', required=False,
                    default='E_mag')
parser.add_argument('-c', '--fn_coil', help='Path to TMS coil', required=True)
parser.add_argument('--opt_search_radius', help='Optimization search radius', default=20, type=float)
parser.add_argument('--opt_search_angle', help='Optimization search angle', default=180, type=float)
parser.add_argument('--opt_angle_resolution', help='Optimization angle resolution', default=7.5, type=float)
parser.add_argument('--opt_spatial_resolution', help='Optimization spatial resolution', default=25, type=float)
parser.add_argument('--opt_smooth', help='Surface smoothing', default=100, type=float)
parser.add_argument('--opt_distance', help='Skin-coil distance', default=1, type=float)
parser.description = 'Determine optimal coil position/orientation for cortical target.\n'
args = parser.parse_args()

# print simulation info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)

print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

fn_subject = os.path.abspath(args.fn_subject)
mesh_idx = args.mesh_idx
exp_idx = args.exp_idx
target = args.target
n_cpu = args.n_cpu
anisotropy_type = args.anisotropy_type
pardiso = args.pardiso
qoi = args.qoi
fn_coil = os.path.abspath(args.fn_coil)
opt_search_radius = args.opt_search_radius
opt_search_angle = args.opt_search_angle
opt_angle_resolution = args.opt_angle_resolution
opt_spatial_resolution = args.opt_spatial_resolution
opt_smooth = args.opt_smooth
opt_distance = args.opt_distance

if len(target) > 1:
    target = np.array(target).astype(float)
    print(f"Defining user defined target: {target}")
else:
    target = target[0]

# determine target coordinates from r2 map
if type(target) is str:
    print(f"Reading target from file: {target}")
    # read r2 data
    fn_r2_data = target
    fn_r2_geo = os.path.splitext(fn_r2_data)[0][:-4] + "geo.hdf5"

    # find element with maximum goodness-of-fit
    with h5py.File(fn_r2_data, 'r') as gof_res:
        best_elm_id = np.argmax(gof_res[f'data/tris/c_{qoi}'][:])

    # get location of best element on cortex
    with h5py.File(fn_r2_geo, 'r') as gof_geo:
        nodes_ids = gof_geo['/mesh/elm/triangle_number_list'][best_elm_id]
        target = np.mean(gof_geo['/mesh/nodes/node_coord'][:][nodes_ids], axis=0)

# Subject information
# ========================================================================================================
subject_dir = os.path.split(fn_subject)[0]
subject = pynibs.load_subject(fn_subject)

# file containing the head model (.msh format)
fn_mesh_msh = subject.mesh[mesh_idx]["fn_mesh_msh"]

assert os.path.exists(fn_coil), f"Coil file '{fn_coil}' is missing."
assert os.path.exists(fn_mesh_msh), f"Mesh file '{fn_coil}' is missing."

optim_dir = os.path.join(subject_dir,
                         f"opt",
                         f"exp_{exp_idx}",
                         f"target_{np.array2string(target, formatter={'float_kind': '{0:+0.2f}'.format})}",
                         f"mesh_{mesh_idx}",
                         f"{qoi}")
if not os.path.exists(optim_dir):
    os.makedirs(optim_dir)

print(f'\ttarget:   {target}')
print(f'\theadmesh: {fn_mesh_msh}')
print(f'\toptim_dir: {optim_dir}')

fn_conform_nii = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], subject.mesh[mesh_idx]["fn_mri_conform"])
try:
    fn_exp_nii = subject.exp[exp_idx]["fn_mri_nii"][0][0]
except KeyError:
    print(f"Experiment {exp_idx} not found.")
    fn_exp_nii = fn_conform_nii

# Initialize structure
# ========================================================================================================
tms_opt = opt_struct.TMSoptimize()
tms_opt.fnamehead = fn_mesh_msh
tms_opt.pathfem = optim_dir
tms_opt.fnamecoil = fn_coil
tms_opt.target = target
tms_opt.anisotropy_type = anisotropy_type

if subject.mesh[mesh_idx]['fn_tensor_vn'] is not None:
    tms_opt.fname_tensor = os.path.join(subject.mesh[mesh_idx]["mesh_folder"],
                                        subject.mesh[mesh_idx]['fn_tensor_vn'])
else:
    tms_opt.fname_tensor = None

if pardiso:
    tms_opt.solver_options = 'pardiso'  # 'pardiso'  # faster, lots of RAM
else:
    tms_opt.solver_options = None
tms_opt.open_in_gmsh = False

tms_opt.search_radius = opt_search_radius
tms_opt.search_angle = opt_search_angle
tms_opt.angle_resolution = opt_angle_resolution
tms_opt.spatial_resolution = opt_spatial_resolution
tms_opt.smooth = opt_smooth
tms_opt.distance = opt_distance

# optimization target area in mm
tms_opt.target_size = .5
mesh = mesh_io.read_msh(fn_mesh_msh)

# check if the target is within GM
# ========================================================================================================

tri_nodes = mesh.nodes.node_coord[mesh.elm.node_number_list[mesh.elm.tag2 == 1002][:, :3] - 1]
tri_bar = np.average(tri_nodes, axis=1)
tri_bar.shape
bar = mesh.elements_baricenters()[:]
dist = np.linalg.norm(bar - target, axis=1)
elm = mesh.elm.elm_number[
    (dist < tms_opt.target_size) *
    np.isin(mesh.elm.tag1, [1002]) *
    np.isin(mesh.elm.elm_type, [2])
    ]
if not len(elm):
    print(f"\tNo elements found at {target}. Changing target.")
    sub_elms = bar[np.isin(mesh.elm.tag1, [1002]) * np.isin(mesh.elm.elm_type, [2])]
    new_sub_coords = sub_elms[np.where(
            np.linalg.norm(tri_bar - target, axis=1)
            ==
            np.min(np.linalg.norm(tri_bar - target, axis=1)))[0][0]]

    tms_opt.target = new_sub_coords
    print(f'\tcoords: {new_sub_coords}')

# read optimal starting mat
opt_mat = pynibs.get_opt_mat(
    # folder='/data/pt_01756/probands/15484.08/opt/exp_FDI_rad15_angle5_allrots_dist0/target_[-31.28 -25.33 +76.40]/mesh_charm_beta/E_mag/')
    # folder='/data/pt_01756/probands/15484.08/opt/exp_FDI_rad10_angle180_allrots_dist0/target_[-31.28 -25.33 +76.40]/mesh_charm_beta/E_mag/')
    folder='/data/pt_01756/probands/15484.08/opt/exp_random_walk/target_[-31.28 -25.33 +76.40]/mesh_charm_beta/E_mag/0014'
)

# Run optimization
# ========================================================================================================
org_optim_dir = optim_dir

#################################
# online fem
##################################
tms_opt.tissues = [1002]
pts = opt_struct.define_target_region(mesh, tms_opt.target, 1, 1002, elm_type=2)
pts = mesh.elm.node_number_list[pts][:, :3].reshape(pts.shape[0] * 3, )
pts = np.unique(pts)
pts = mesh.nodes.node_coord[pts - 1]
roi_simnibs = simnibs.RegionOfInterest(points=pts, mesh=mesh)
onlinefem_solver = simnibs.OnlineFEM(
        mesh=mesh,
        method="TMS",
        roi=roi_simnibs,
        anisotropy_type="scalar",
        solver_options="pardiso",
        fn_results=os.path.join(optim_dir, "e.hdf5"),
        useElements=False,
        fn_coil=fn_coil,
        dataType=0
)

for i in range(2,100):
    print("=" * 64)
    print(f"Starting iteration {i:0>4} out of 1000")
    optim_dir = F"{org_optim_dir}/{i:0>4}"
    os.makedirs(optim_dir, exist_ok=True)
    # tms_opt._prepare()

    # fn_mesh_hdf5="/data/pt_01756/probands/15484.08/mesh/charm_beta/m2m_15484.08/15484.08.hdf5"
    ad = 3
    pos_matrices = pynibs.random_walk_coil(opt_mat, n_steps=500, fn_mesh_hdf5=fn_mesh_msh.replace('.msh', '.hdf5'),
                                           angles_dev=ad,
                                           distance_low=1, distance_high=4,
                                           angles_metric='deg',
                                           coil_pos_fn=os.path.join(optim_dir, "random_coil_walk.hdf5"))
    print(f"{pos_matrices.shape[2]} coil positions found.")
    # tms_opt.pos_ydir = []
    # tms_opt.centre = []
    pos_matrices = np.swapaxes(pos_matrices, 0, 2)
    pynibs.write_coil_pos_hdf5(fn_hdf=os.path.join(optim_dir, "search_positions.hdf5"),
                               centers=np.vstack([p.T[0:3, 3] for p in pos_matrices]),
                               m0=np.vstack([p.T[0:3, 0] for p in pos_matrices]),
                               m1=np.vstack([p.T[0:3, 1] for p in pos_matrices]),
                               m2=np.vstack([p.T[0:3, 2] for p in pos_matrices]),
                               datanames=None, data=None, overwrite=True)

    pos_matrices = np.swapaxes(pos_matrices, 0, 2)
    onlinefem_solver.fn_results = os.path.join(optim_dir, "e.hdf5")
    # coil_pos = np.moveaxis(np.array(pos_matrices), 0, 2)
    es = np.squeeze(onlinefem_solver.update_field(matsimnibs=pos_matrices, didt=1 * 1e6))
    # es.shape = [n_sim, n_elm_in_roi]
    ####################################################

    # opt_matsim = np.squeeze(tms_opt.run(allow_multiple_runs=True, cpus=n_cpu))
    #
    # if len(opt_matsim.shape) == 2:
    #     opt_matsim = opt_matsim[:, :, np.newaxis]
    #
    # matlocalite = pynibs.nnav2simnibs(fn_exp_nii=fn_exp_nii,
    #                                   fn_conform_nii=fn_conform_nii,
    #                                   m_nnav=opt_matsim,
    #                                   target='nnav',
    #                                   nnav_system='localite')

    # write instrument marker file
    # write_tms_navigator_im(matlocalite, os.path.join(optim_dir, 'opt_coil_pos.xml'),  overwrite=True)
    # write_tms_navigator_im(matlocalite, os.path.join(optim_dir, 'opt_coil_pos.xml'), names='opt', overwrite=True)
    # pynibs.create_stimsite_from_matsimnibs(os.path.join(optim_dir, 'opt_coil_pos.hdf5'), opt_matsim, overwrite=True)

    fn_search_positions = os.path.join(optim_dir, 'search_positions.hdf5')
    with h5py.File(fn_search_positions, "r") as f:
        centers = f["centers"][:]
        m0 = f["m0"][:]
        m1 = f["m1"][:]
        m2 = f["m2"][:]
        search_mats = f["matsimnibs"][:]
    os.unlink(fn_search_positions)
    fn = os.path.join(optim_dir, 'e.hdf5')
    # get e-fields at target for each coil position
    # with h5py.File(fn_e, "r") as f:
    #     e = f["e/roi_0/"][:]  # SimNIBS 3
    print(f"data.shape = {es.shape}")
    es = np.average(es, axis=1)
    print(f"data.shape = {es.shape}")
    print(f"centers.shape = {centers.shape[0]}")
    pynibs.write_coil_pos_hdf5(fn_hdf=fn_search_positions,
                               centers=centers,
                               m0=m0,
                               m1=m1,
                               m2=m2,
                               datanames=["E_mag"],
                               data=es,
                               overwrite=True)

    pynibs.write_coil_sequence_xdmf(f"{optim_dir}/random_coil_walk.hdf5", data=es,
                                    vec1=pos_matrices[:3, 0, :].T,
                                    vec2=pos_matrices[:3, 1, :].T,
                                    vec3=pos_matrices[:3, 2, :].T,
                                    # nodes_idx=np.arange(n_steps),
                                    output_xdmf=f"{optim_dir}/random_coil_walk.xdmf")

    print("=== DONE ===")
