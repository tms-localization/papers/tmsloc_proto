#!/usr/bin/env python

"""
Computes the resting motor threshold (rMT) based on data from regression experiment.

example files:
fn_c : example_data/subject_0/results/exp_m1/r2/mesh_mesh0_refinedM1/roi_midlayer_m1s1pmd/FDI/sigmoid4_log/r2_roi_data.hdf5
fn_e : example_data/subject_0/results/exp_m1/electric_field/mesh_mesh0_refinedM1/roi_midlayer_m1s1pmd/e_scaled.hdf5
fn_exp : example_data/subject_0/exp/m1/experiment.hdf5
fn_e_opt :
  example_data/subject_0/opt/exp_m1/target_[-26.56 -38.13 +57.77]/mesh_mesh0_refinedM1/E_mag/subject_0_TMS_optimize_MagVenture_MCF_B65_REF_highres.ccd_nii.hdf5
 or:
  fn_e_opt : example_data/subject_0/opt/exp_m1/target_[-26.56 -38.13 +57.77]/mesh_mesh0_refinedM1/E_mag/coil_positions.geo
"""

import os
import re
import h5py
import pynibs
import argparse
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pynibs.regression import regress_data


parser = argparse.ArgumentParser()
parser.add_argument('--fn_gof', help='Path to goodness-of-fit results .hdf5 file (ROI)', required=True)
parser.add_argument('--fn_e', help='Path to e_scaled.hdf5 file (ROI)', required=True)
parser.add_argument('--fn_exp', help='Path to experiment.hdf5 file', required=True)
parser.add_argument('--fn_e_opt', help='Path to optimal field .hdf5 or coil_positions.geo', required=True)
parser.add_argument('--muscle', help='Muscle name', default='FDI')
parser.add_argument('--fun_type', help='Function to fit data. One of ("sigmoid", "sigmoid4", "sigmoid4_log", "linear")',
                    default='sigmoid4')
parser.add_argument('--MSO_factor', help='Vendor specific factor to tranform A/us to MSO in percent', default=1.43)
parser.add_argument('--rmt_thres', help='rMT threshold in uV', default=50)
parser.description = 'Compute the resting motor threshold (rMT) based on data from regression experiment.\n' \
                     '\t1) The winning cortical location (= max(gof)) is identified,\n' \
                     '\t2) |E| is regressed on MEP at this element,\n' \
                     '\t3) and finally the rMT threshold (=50uV) field intensity is identified, transformed to %MSO' \
                     ' and printed.\n'


args = parser.parse_args()

# print simulation info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)

print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

fn_gof = os.path.abspath(args.fn_gof)
fn_e = os.path.abspath(args.fn_e)
fn_exp = os.path.abspath(args.fn_exp)
fn_e_opt = os.path.abspath(args.fn_e_opt)
fn_plot = os.path.join(os.path.split(fn_e_opt)[0], "fit_in_hotspot_rmt_estimation.png")

# some default parameters
muscle = args.muscle
intensity_factor = float(args.MSO_factor)  	# intensity scale factor A/us per %MSO
rmt_thres = float(args.rmt_thres) 		# rMT MEP threshold in µV

if args.fun_type == 'sigmoid':
    fun = pynibs.sigmoid
elif args.fun_type == 'sigmoid4':
    fun = pynibs.sigmoid4
elif args.fun_type == 'sigmoid4_log':
    fun = pynibs.sigmoid4_log
elif args.fun_type == 'linear':
    fun = pynibs.linear
else:
    raise NotImplementedError(f"{args.funtype} unknown. ")

# load e-field in hotspot
if fn_e_opt.endswith('.hdf5'):
    with h5py.File(fn_e_opt, "r") as f:
        try:
            e_opt = np.max(f["tms_optimization/E_magn"][:])#
        except KeyError:
            e_opt = np.max(f["tms_optimization/E_norm"][:])
elif fn_e_opt.endswith('.geo'):
    # read e-field from .pos file
    # line starting with 'SP'(float, float, float) {e_mag};
    exp = r"SP\([+-]?[0-9]*[.]?[0-9]+, [+-]?[0-9]*[.]?[0-9]+, [+-]?[0-9]*[.]?[0-9]+\){([+-]?[0-9]*[.]?[0-9]+)};"
    with open(fn_e_opt, 'r') as f:
        e = []
        for line in f:
            try:
                e.append(float(re.findall(exp, line)[0]))
            except IndexError:
                pass
    e_opt = np.array(e).max()
else:
    raise ValueError

# load congruence factor map and read optimal element index
with h5py.File(fn_gof, "r") as f:
    opt_ele_idx = np.argmax(f["data/tris/c_E_mag"][:])

# load MEPs
df_mep = pd.read_hdf(fn_exp, "phys_data/postproc/EMG")
mep = np.array(df_mep[f"p2p_{muscle}"])

# load e-fields in hotspot
with h5py.File(fn_e, "r") as f:
    e = f["E_mag"][:, opt_ele_idx][:, np.newaxis]

# check for zero e-fields and filter them (problems in FEM!)
zero_mask = (e == 0).flatten()

if zero_mask.any():
    print(f"Warning! {np.sum(zero_mask)} zero e-fields detected in element! Check FEM! Ignoring them for now!")
    e = e[np.logical_not(zero_mask)]
    mep = mep[np.logical_not(zero_mask)]
    
# perform fit in hotspot (again) to get IO curve
c, fit = regress_data(e_matrix=e,
                      mep=mep,
                      fun=fun,
                      n_cpu=1,
                      n_refit=50,
                      return_fits=True,
                      refit_discontinuities=False)

print(f"Element {opt_ele_idx} identified with max(GOF)={np.round(c[0],2)}")

# determine rmt_thres uV threshold in V/m
x = np.linspace(np.min(e), np.max(e), 1000)

if args.fun_type == 'sigmoid4_log':
    y = 10 ** fun(x, **fit[0])
else:
    y = fun(x, **fit[0])

# determine RMT
rmt = (x[y > rmt_thres/1000][0] / e_opt) / intensity_factor
e_threshold = (x[y > rmt_thres/1000][0])

# plot fit and highlight RMT
plt.figure()
plt.scatter(e, mep, s=4, c='k')
plt.plot(x, y, linewidth=3)
plt.legend([f"Fit ({args.fun_type})", "Measurements"], loc="upper left")
plt.plot([np.min(x), e_threshold], [rmt_thres/1000, rmt_thres/1000], "r--", linewidth=2)
plt.plot([e_threshold, e_threshold], [0, rmt_thres/1000], "r--", linewidth=2)
plt.xlabel("Electric field in V/m", fontsize=11)
plt.ylabel("MEP in mV", fontsize=11)
plt.grid()
plt.title(f"rMT for MEP>{rmt_thres}µV: {np.round(e_threshold,2)} V/m, {np.round(rmt,2)} %MSO")
plt.savefig(fn_plot, dpi=600)

print(f"rMT computed for {rmt_thres}µV: {np.round(rmt,2)} %MSO")
print(f"Electric field threshold computed for {rmt_thres}µV: {np.round(e_threshold,2)} V/m")
print(f"Saved plot in : {fn_plot}")
