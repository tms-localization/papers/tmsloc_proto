% Builds a freesurfer overlay .mgh file to define a
% cortical region of interest (ROI).
%
% This follows a two-step procedure:
% 1. Define a set of BA labs
% 2. Intesect with a set of manually defined vertices
% 3. Write .mgh file
%
% This script is part of the repository for our protocol 'Precise
% motor-mapping with transcranial magnetic stimulation'.
% Feel free to adjust to your needs.
%
% Numssen, O., Weise, K., Kalloch, B., Zier, A. L., Thielscher, A.,
% Hartwigsen, G., & Knösche, T. R. (2022, June 9).
% Precise motor-mapping with transcrianial magnetic stimulation - data and code.
% https://doi.org/10.17605/OSF.IO/MYRQN

fs_subject_dir = getenv('SUBJECTS_DIR');
roi_fn = 'lefthandknob_M1S1PMd.overlay';
%% Step 1 -- Brodmann Areals
% Which BA areas to use
areas = {'Brodmann.6', 'Brodmann.4', 'Brodmann.3', 'Brodmann.1'};
[vertices, label, colortable] = read_annotation([fs_subject_dir '/fsaverage/label/lh.PALS_B12_Brodmann.annot']);

idx_areas=zeros(size(areas));
for i=1:length(areas)
    for j=1:length(colortable.struct_names)
        if strcmpi(areas{i},colortable.struct_names{j})
            idx_areas(i)=j;
            break
        end
    end
end

% Get indices of BAs
overlay = zeros(size(vertices));

ourvertices = false(size(vertices));
idx_label = colortable.table(idx_areas,5);
for i=1:length(idx_label)
    ourvertices(label==idx_label(i))=true;
end
ourvertices=vertices(ourvertices)+1; % matlab indexing

overlay(ourvertices)=1;

%% Step 2 -- Manual reference points
% cut out area around handknob
max_dist = 20; % maximal distance in [mm] to one of the reference points

% Provide vertex coordinates in coordinate space of the correct file (pial,
% inf, etc).
refpts = [ -8.9259, 43.1942, 41.9060;...
           -4.8111, 33.4609, 48.9409;...
           -8.3797, 35.4663, 45.8545;...
           -6.9600,  3.1700, 56.6900;...
           -2.8600, 13.8200, 56.7300;...
          -10.3100, -1.8900, 55.5100;...
          -13.1200, -8.0900, 54.3800;...
          -14.0900,  0.2100, 51.7700];


% Alternatively, you can provide the vertex indices, for example from
% manual selection from FreeView, and grab the coordinates like this:
% vertex_coords(idx,:)

[vertex_coords, faces] = read_surf([fs_subject_dir '/fsaverage/surf/lh.inflated']);
% [vertex_coords, faces] = read_surf([fs_subject_dir '/fsaverage/surf/lh.pial']);


vertexIsClose=false(size(vertices));
for i=1:size(refpts,1)
    vertexIsClose = vertexIsClose | sum(bsxfun(@minus, vertex_coords, refpts(i,:)).^2, 2) <= max_dist^2;
end

% Intersection of BA and reference points
overlay=overlay&vertexIsClose;
disp([num2str(sum(overlay)) ' vertices found in ROI.' ]);

%% Write file
disp(['Writing to ' pwd '/' roi_fn]);
write_curv(roi_fn, overlay, length(vertices));
