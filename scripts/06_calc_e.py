#!/usr/bin/env python

"""
This script calculates all E-fields for an experiment.
Additionally, all fields are interpolated to the GM-EM midlayer ROI.
"""

import os
import sys
import time
import h5py
import pynibs
import pathlib
import warnings
import argparse
import numpy as np
import pandas as pd
from scipy.io import loadmat
from simnibs.optimization import opt_struct
from simnibs.simulation import save_matlab_sim_struct

parser = argparse.ArgumentParser(description='Calculate many E-fields and save midlayer E in ROI in .hdf5 file\n')

# required arguments
parser.add_argument('-s', '--fn_subject', help='Path to subject obj',
                    required=True, type=str)
parser.add_argument('-m', '--mesh_idx', help='Mesh idx',
                    required=True, type=str, default="reg")
parser.add_argument('-e', '--exp_idx', help='Exp idx',
                    required=True, type=str, default="reg_isi_05")
parser.add_argument('-r', '--roi_idx', help='ROI idx',
                    required=False, type=str, default="midlayer_m1s1pmd")
parser.add_argument('-n', '--n_cpu', help='How many cpus to use',
                    type=int, default=10)
parser.add_argument('-a', '--anisotropy_type', help='Anisotropy "vn" or "scalar". '
                                                    '"scalar" uses isotropic tissue conductivities.',
                    type=str, default="vn")
parser.add_argument('-p', '--pardiso', dest='pardiso', action='store_true', help="Use pardiso solver.")
parser.add_argument('--fail_threshold', type=float, default=5,
                    help="Maximum percentage of not-computed fields.")
parser.add_argument('--recompute', dest='recompute', action='store_true',
                    help="Force (re-)compution of fields if valid e.hdf is found.")
parser.add_argument('--calc_theta_grad', default=False, action='store_true',
                    help='Have the E-field angle to surface normal (theta) and E-field gradient between'
                         'the gray and white matter calculated. Required for neuronal meanfield based mapping.')
parser.add_argument('-o', '--online', dest='online', action='store_true', default=False,
                    help="Use fast OnlineFEM.")
args = parser.parse_args()

# print simulation info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)

print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

# set file names and parameters
# ========================================================================================================
# file containing the subject information
fn_subject = os.path.abspath(args.fn_subject)

# load subject object
subject = pynibs.load_subject(fn_subject)

# exp identifier
exp_idx = args.exp_idx

# mesh identifier
mesh_idx = args.mesh_idx

# ROI index the fields are extracted for goodness-of-fit calculation
roi_idx = args.roi_idx

# force recompute?
recompute = args.recompute

# output folder
fn_out = os.path.join(os.path.split(fn_subject)[0], 'results',
                      f"exp_{exp_idx}", "electric_field",
                      f"mesh_{mesh_idx}",
                      f"roi_{roi_idx}")

# file containing the head model (.msh format)
fn_mesh_msh = subject.mesh[mesh_idx]["fn_mesh_msh"]

# file containing the conductivity tensors
fn_tensor_vn = subject.mesh[mesh_idx]["fn_tensor_vn"]

# file containing the magnetic vector potential of the TMS coil
fn_coil = subject.exp[exp_idx]["fn_coil"][0][0]

subject.exp[exp_idx]["fn_exp_hdf5"] = [os.path.join(subject.subject_folder, "exp", exp_idx, f"mesh_{mesh_idx}",
                                                    "experiment.hdf5")]

# file containing the coil positions
if 'fn_matsimnibs' in subject.exp[exp_idx].keys():
    fn_matsimnibs = subject.exp[exp_idx]['fn_matsimnibs']
else:
    fn_matsimnibs = "matsimnibs.hdf5"
fn_coilpos_hdf5 = os.path.join(os.path.split(subject.exp[exp_idx]["fn_exp_hdf5"][0])[0], fn_matsimnibs)

# file containing the FEM options for SimNIBS (will be created)
fn_fem_options = os.path.join(fn_out, 'FEM_config.mat')

# anisotropy type ("scalar" or "vn" for volume normalized)
anisotropy_type = args.anisotropy_type
if os.name == 'nt':  # 'nt' is Windows
    warnings.warn(f"anisotropy_type={anisotropy_type} needs diffusion tensory, which cannot be created "
                  f"with Windows at the moment. Is this really what you want?")
    time.sleep(3)

# number threads (cpu cores) to use for parallelization
n_cpu = args.n_cpu

# use pardiso solver
pardiso = args.pardiso

# use fast OnlineFEM (calls script run_simnibs_online.py)
online = args.online

if online:
    try:
        from simnibs.simulation.onlinefem import OnlineFEM
    except ImportError:
        raise NotImplementedError("OnlineFEM not implemented in the installed SimNIBS version.")

# Set up SimNIBS
# ========================================================================================================
if anisotropy_type != 'scalar':
    if not os.path.exists(os.path.join(subject.mesh[mesh_idx]["mesh_folder"], fn_tensor_vn)):
        print("No conductivity tensor data available ... changing to anisotropy_type='scalar'")
        anisotropy_type = "scalar"

if not online:
    tms_opt = opt_struct.TMSoptimize()
    tms_opt.fnamehead = fn_mesh_msh
    tms_opt.pathfem = fn_out
    tms_opt.fnamecoil = fn_coil
    tms_opt.target = None
    tms_opt.open_in_gmsh = False
    tms_opt.anisotropy_type = anisotropy_type

    if pardiso:
        tms_opt.solver_options = 'pardiso'  # faster, eats lots of RAM
    else:
        tms_opt.solver_options = None

    if anisotropy_type != 'scalar':
        tms_opt.fname_tensor = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], fn_tensor_vn)

    if not os.path.exists(fn_out):
        os.makedirs(fn_out)

    # Save FEM options
    # ========================================================================================================
    # let's check if an old fn_fem_options is already existing:
    if os.path.exists(fn_fem_options) and not recompute:
        print(f"Found old FEM config file {fn_fem_options}. Checking for differences...")
        try:
            # check if old config file has same options set
            tms_opt_old = opt_struct.TMSoptimize()
            tms_opt_old = tms_opt_old.read_mat_struct(loadmat(fn_fem_options))
            for k, v in tms_opt_old.__dict__.items():
                if k not in ['date', 'time_str'] and v != '':
                    assert v == tms_opt.__dict__[k], print(
                            f"Difference found for '{k}': Old: {v}. New: {tms_opt.__dict__[k]}")
            print(f"No differences found. Using old FEM config.")
        except AssertionError:
            recompute = True
            save_matlab_sim_struct(tms_opt, fn_fem_options)
    else:
        save_matlab_sim_struct(tms_opt, fn_fem_options)

roi = pynibs.load_roi_surface_obj_from_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])[roi_idx]
n_tris_roi = roi.n_tris

with h5py.File(fn_coilpos_hdf5, 'r') as f:
    n_coils = f['/matsimnibs'].shape[2]

# Run electric field simulations
# ========================================================================================================
fn_e = os.path.join(fn_out, f"e.hdf5")
field_fail_threshold = args.fail_threshold
if not recompute and os.path.exists(fn_e):
    print(f"Old field simulations found: {fn_e}. Checking file.")
    with h5py.File(fn_e, 'a') as f:
        try:
            if 'tmp' in f.keys():
                n_fields = f['tmp'].shape[0]
                assert f['tmp'].shape == (n_coils, n_tris_roi, 6), \
                    f"Wrong shape {f['tmp'].shape} found, expected {(n_coils, n_tris_roi, 6)}. Recomputing fields."
                n_zero_fields = np.sum(np.sum(np.sum(f['tmp'], axis=2), axis=1) == 0)
                perc_zero_fields = np.round(n_zero_fields / (n_fields / 100), 2)
                if n_zero_fields:
                    print(f"{n_zero_fields} empty fields (of {n_fields} total fields) found ({perc_zero_fields}%).")
                if perc_zero_fields >= field_fail_threshold:
                    print(f"Too many empty fields found.")
                    print(f"This can happen if not enough ressources are available for the computation. \n"
                          f"Re-run this script with less CPUs (currently: '--n_cpu={n_cpu}') "
                          f"and the '--recompute' flag.\n"
                          f"Or, increase '--fail_threshold' (currently: {field_fail_threshold}) to at least "
                          f"{perc_zero_fields} to remove empty fields from the data.\nQuitting.")
                    quit()

                assert np.all(np.sum(np.sum(f['tmp'], axis=2), axis=1) != 0), f"Empty field found, recomputing fields."
                if 'E' not in f.keys():
                    print(f"Reshaping fields.")
                    E = f['tmp'][:, :, 0:3]
                    E_mag = f['tmp'][:, :, 3]
                    E_norm = f['tmp'][:, :, 4]
                    E_tan = f['tmp'][:, :, 5]
                    f.create_dataset('/E', data=E)
                    f.create_dataset('/E_mag', data=E_mag)
                    f.create_dataset('/E_norm', data=E_norm)
                    f.create_dataset('/E_tan', data=E_tan)
                    if args_dict["calc_theta_grad"]:
                        E_theta = f['tmp'][:, :, 6]
                        E_grad = f['tmp'][:, :, 7]
                        f.create_dataset('/E_theta', data=E_theta)
                        f.create_dataset('/E_grad', data=E_grad)

            elif 'E' in f.keys():
                assert f['E'].shape == (n_coils, n_tris_roi, 3), \
                    f"Field component E: Wrong shape {f['E'].shape} found, expected {(n_coils, n_tris_roi, 3)}. " \
                    f"Recomputing fields."
                for k in ['E_mag', 'E_norm', 'E_tan']:
                    assert f[k].shape == (n_coils, n_tris_roi), \
                        f"Field component {k}: Wrong shape {f[k].shape} found, expected {(n_coils, n_tris_roi)}. " \
                        f"Recomputing fields."
            print("Test ok, using existing field simulations.")
        except AssertionError as e:
            print(e)
            recompute = True
else:
    recompute = True

scripts_folder = pathlib.Path(__file__).parent.absolute()

# compute FEMs
if online:
    fn_e = os.path.join(fn_out, f"e_scaled.hdf5")
    if recompute:
        cmd = f"{sys.executable} {os.path.join(scripts_folder, 'run_simnibs_online.py')} " \
              f"--folder '{fn_out}' " \
              f"--fn_subject '{fn_subject}' " \
              f"--fn_coilpos '{fn_coilpos_hdf5}' " \
              f"--anisotropy_type {anisotropy_type} " \
              f"-m '{mesh_idx}' " \
              f"--fn_coil '{fn_coil}' " \
              f"-r {roi_idx} " \
              f"--hdf5_fn '{fn_e}' " \
              f"--fn_exp '{subject.exp[exp_idx]['fn_exp_hdf5'][0]}'"
        cmd += (" --calc_theta_grad" if args_dict["calc_theta_grad"] else "")
        print(f"Running {cmd}")
        os.system(cmd)
else:
    if recompute:
        cmd = f"{sys.executable} {os.path.join(scripts_folder, 'run_simnibs.py')}" \
              f" --folder {fn_out} " \
              f"--fn_subject {fn_subject} " \
              f"--fn_coilpos {fn_coilpos_hdf5}" \
              f" --cpus {n_cpu} " \
              f"--anisotropy_type {anisotropy_type} " \
              f"-m {mesh_idx} " \
              f" -r '{roi_idx}' " \
              f"--hdf5_fn '{fn_e}'"
        cmd += (" --calc_theta_grad" if args_dict["calc_theta_grad"] else "")
        print(f"Running {cmd}")
        os.system(cmd)

        with h5py.File(fn_e, "a") as f:
            print(f"Reshaping fields.")
            E = f['tmp'][:, :, 0:3]
            E_mag = f['tmp'][:, :, 3]
            E_norm = f['tmp'][:, :, 4]
            E_tan = f['tmp'][:, :, 5]
            f.create_dataset('/E', data=E)
            f.create_dataset('/E_mag', data=E_mag)
            f.create_dataset('/E_norm', data=E_norm)
            f.create_dataset('/E_tan', data=E_tan)
            if args_dict["calc_theta_grad"]:
                E_theta = f['tmp'][:, :, 6]
                E_grad = f['tmp'][:, :, 7]
                f.create_dataset('/E_theta', data=E_theta)
                f.create_dataset('/E_grad', data=E_grad)

    # Scale electric field with coil current and save it under e_scaled.hdf5
    # ========================================================================================================
    # read coil current
    df_stim_data = pd.read_hdf(subject.exp[exp_idx]["fn_exp_hdf5"][0], "stim_data")
    didt = np.array(df_stim_data["current"])

    # read and scale electric fields
    with h5py.File(fn_e, "r") as f:
        e_scaled = dict()
        for e_qoi_path in f.keys():
            if "E" in e_qoi_path:
                print(e_qoi_path)

                # The angle of the E-field with respect to the local surface normal as well as the
                # change of the efield across the local GM crossection should be be scaled as they
                # are unaffected by a scaling of the underlying E-field.
                if e_qoi_path not in ["E_gradient", "E_theta"]:
                    e_scaled[e_qoi_path] = f[e_qoi_path][:] * np.expand_dims(didt, axis=tuple(
                            np.arange(f[e_qoi_path][:].ndim - 1) + 1))
                else:
                    e_scaled[e_qoi_path] = f[e_qoi_path][:]

    # save scaled electric fields
    fn_scaled = os.path.join(fn_out, f"e_scaled.hdf5")
    print(f"Writing scaled fields to {fn_scaled}.")
    with h5py.File(fn_scaled, "w") as f:
        for e_qoi_path in e_scaled.keys():
            f.create_dataset(e_qoi_path, data=e_scaled[e_qoi_path])

    # check for any zero rows in simulations
    print(e_scaled.keys())
    n_zero_simulations = np.sum(np.linalg.norm(e_scaled[list(e_scaled.keys())[0]], axis=1) == 0)
    if n_zero_simulations > 0:
        print(f"WARNING: {n_zero_simulations}/{n_coils} failed simulations detected! "
              f"Reduce n_cpu and avoid memory overflows to avoid this!")

print("06_calc_e.py: Done.")
