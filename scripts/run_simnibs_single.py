#!/usr/bin/env python
"""
This uses the SimNIBS TMS optimize methods to calculate many electric field distributions.
"""
import os
import h5py
import pynibs
import simnibs
import argparse
import numpy as np

parser = argparse.ArgumentParser(description='Perform E-field calculation with SimNIBS')

# required arguments
parser.add_argument('-s', '--fn_subject',      help='Subject .hdf5 file', required=True, type=str)
parser.add_argument('-m', '--mesh_idx',        help='Mesh ID', required=True, type=str)
parser.add_argument('-r', '--roi_idx',         help='ROI ID', required=True, type=str)
parser.add_argument('--fn_coil',               help='Filename of TMS coil .nii.gz file with magnetic field', required=True, type=str)
parser.add_argument('--fn_coilpos',            help='Filename of TMS position .hdf5', required=True, type=str)
parser.add_argument('--fn_results_folder',     help='Results folder for the E fields', default="e.hdf5", type=str)
parser.add_argument('-a', '--anisotropy_type', help='Anisotropy type: "vn" or "scalar"', type=str, default="vn")
parser.add_argument('--solver_opt', help='SimNIBS solver options', default='', type=str)
args = parser.parse_args()

# import after setting environment solver options
os.environ['SIMNIBS_SOLVER_OPTIONS'] = ""

# set parameters
# ================================================================================
midlayer_fun = "simnibs"
mesh_idx = args.mesh_idx
roi_idx = args.roi_idx
fn_coil = args.fn_coil
solver_options = args.solver_opt
fn_coilpos = args.fn_coilpos
fn_results_folder = args.fn_results_folder
anisotropy_type = args.anisotropy_type
subject = pynibs.load_subject_hdf5(args.fn_subject)
phi_scaling = 1

# Setting up SimNIBS SESSION
# ========================================================================================================
print("Setting up SimNIBS SESSION")
S = simnibs.sim_struct.SESSION()
S.fnamehead = subject.mesh[mesh_idx]["fn_mesh_msh"]
S.pathfem = fn_results_folder
S.fields = "vDeE"
S.open_in_gmsh = False
S.fname_tensor = None
S.map_to_surf = True
S.map_to_fsavg = True
S.map_to_MNI = True
S.subpath = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], f"m2m_{subject.id}")

if not os.path.exists(S.pathfem):
    os.makedirs(S.pathfem)

# Define the TMS simulation and setting up conductivities
# ========================================================================================================
tms = [S.add_tmslist()]
tms[0].fnamecoil = fn_coil
tms[0].cond[0].value = 0.126  # WM
tms[0].cond[1].value = 0.275  # GM
tms[0].cond[2].value = 1.654  # CSF
tms[0].cond[3].value = 0.01   # Skull
tms[0].cond[4].value = 0.465  # Scalp
tms[0].anisotropy_type = anisotropy_type
tms[0].solver_options = solver_options

if subject.mesh[mesh_idx]['fn_tensor_vn'] is not None:
    tms[0].fn_tensor_nifti = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], subject.mesh[mesh_idx]['fn_tensor_vn'])
else:
    tms[0].fn_tensor_nifti = None

tms[0].excentricity_scale = None

# Define the coil position
# ========================================================================================================
with h5py.File(fn_coilpos, "r") as f:
    matsimnibs = f["matsimnibs"][:]

if matsimnibs.ndim == 2:
    matsimnibs = matsimnibs[:, :, np.newaxis]

# matsimnibs = np.repeat(matsimnibs, 1, axis=2)

for i_pos in range(matsimnibs.shape[2]):
    pos = tms[0].add_position()
    pos.matsimnibs = matsimnibs[:, :, i_pos]
    pos.distance = 0.1
    pos.didt = 1e6  # coil current rate of change in A/s (1e6 A/s = 1 A/us)
    pos.postprocess = S.fields

# Running simulations
# ========================================================================================================
print("Running SimNIBS")
filenames = simnibs.run_simnibs(S, cpus=1)

print("Saving results in .hdf5 format.")
if type(filenames) is not list:
    filenames = [filenames]

pynibs.simnibs_results_msh2hdf5(fn_msh=filenames,
                                fn_hdf5=[os.path.join(S.pathfem, os.path.split(f)[1]) for f in filenames],
                                S=S,
                                pos_tms_idx=[0],    # indices of different "coils"
                                pos_local_idx=[i for i in range(len(filenames))],  # indices of associated positions
                                subject=subject,
                                mesh_idx=mesh_idx,
                                overwrite=True,
                                mid2roi=True,
                                verbose=True)
print("=== DONE ===")
