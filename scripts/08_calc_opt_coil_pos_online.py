#!/usr/bin/env python

"""
Determine optimal coil position/orientation from localization experiment.
"""

import os
import re
import sys
import h5py
import time
import pynibs
import pathlib
import simnibs
import warnings
import shutil
import argparse
import numpy as np
from simnibs import opt_struct, mesh_io
from simnibs.utils.nnav import localite
from pynibs.neuron.neuron_regression import calc_e_effective

loc = localite()
write_tms_navigator_im = loc.write

# set up command line argument parser
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--fn_subject', help='Path to *.hdf5-file of subject', required=True)
parser.add_argument('-m', '--mesh_idx', help='Mesh ID', required=True, type=str)
parser.add_argument('-r', '--roi_idx', help='ROI idx',
                    required=False, type=str, default="midlayer_m1s1pmd")
parser.add_argument('-e', '--exp_idx', help='Exp idx',
                    required=True, type=str)
parser.add_argument('-a', '--anisotropy_type',
                    help='Anisotropy "vn" or "scalar"', type=str, default="vn")
parser.add_argument('-t', '--target', nargs='+', required=True,
                    help='R2 results file or x/y/z coordinates')
parser.add_argument('--target_radius',
                    required=False,
                    help='Radius around target where e-field is averaged.',
                    default=1)
parser.add_argument('-q', '--qoi',
                    help='Electric field component ("E_mag", "E_norm", "E_tan")', required=False,
                    default='E_mag')
parser.add_argument('-c', '--fn_coil',
                    help='Path to TMS coil', required=True)
parser.add_argument('--opt_search_radius',
                    help='Optimization search radius', default=20, type=float)
parser.add_argument('--opt_search_angle',
                    help='Optimization search angle', default=180, type=float)
parser.add_argument('--opt_angle_resolution',
                    help='Optimization angle resolution', default=7.5, type=float)
parser.add_argument('--opt_spatial_resolution',
                    help='Optimization spatial resolution', default=3, type=float)
parser.add_argument('--opt_smooth', help='Surface smoothing', default=100, type=float)
parser.add_argument('--opt_distance', help='Skin-coil distance', default=1, type=float)
parser.add_argument('--layerid',
                    help='Perform mapping based on neuronal meanfield model on the specified layer. ("L23", "L5")',
                    required=False)
parser.add_argument('--neuronmodel', help='Select neuron model for localization. '
                                          '("sensitivity_weighting", "threshold_subtract", "threshold_binary",'
                                          ' "IOcurve")',
                    required=False, default=None)
parser.add_argument('--waveform', help='Waveform of TMS pulse. ("monophasic", "biphasic")',
                    required=False, default='biphasic')
parser.add_argument('--cosine_model', dest='cosine_model', action='store_true',
                    help='only consider the normal component of E-field')
parser.description = 'Determine optimal coil position/orientation for cortical target.\n'
args = parser.parse_args()
np.set_printoptions(suppress=True)

scripts_folder = pathlib.Path(__file__).parent.absolute()

# print simulation info
# ================================================================================
print("-" * 64)
print(f"{parser.description}")
args_dict = vars(args)

print('Parameters:')
for key in args_dict.keys():
    print(f"{key: >15}: {args_dict[key]}")
print("-" * 64)

fn_subject = os.path.abspath(args.fn_subject)
mesh_idx = args.mesh_idx
roi_idx = args.roi_idx
target = args.target
exp_idx = args.exp_idx
target_radius = float(args.target_radius)
anisotropy_type = args.anisotropy_type
qoi = args.qoi
fn_coil = os.path.abspath(args.fn_coil)
opt_search_radius = args.opt_search_radius
opt_search_angle = args.opt_search_angle
opt_angle_resolution = args.opt_angle_resolution
opt_spatial_resolution = args.opt_spatial_resolution
opt_smooth = args.opt_smooth
opt_distance = args.opt_distance
layerid = args.layerid
neuronmodel = args.neuronmodel
waveform = args.waveform
cosine_model = args.cosine_model

if len(target) > 1:
    target = np.array(target).astype(float)
    print(f"Defining user defined target: {target}")
else:
    target = target[0]

# determine target coordinates from r2 map
if type(target) is str:
    print(f"Reading target from file: {target}")
    # read r2 data
    fn_r2_data = target
    fn_r2_geo = os.path.splitext(fn_r2_data)[0][:-4] + "geo.hdf5"

    # find element with maximum goodness-of-fit
    with h5py.File(fn_r2_data, 'r') as gof_res:
        best_elm_id = np.argmax(gof_res[f'data/tris/c_{qoi}'][:])

    # get location of best element on cortex
    with h5py.File(fn_r2_geo, 'r') as gof_geo:
        nodes_ids = gof_geo['/mesh/elm/triangle_number_list'][best_elm_id]
        target = np.mean(gof_geo['/mesh/nodes/node_coord'][:][nodes_ids], axis=0)

# Subject information
# ========================================================================================================
subject_dir = os.path.split(fn_subject)[0]
subject = pynibs.load_subject(fn_subject)

# file containing the head model (.msh format)
fn_mesh_msh = subject.mesh[mesh_idx]["fn_mesh_msh"]

assert os.path.exists(fn_coil), f"Coil file '{fn_coil}' is missing."
assert os.path.exists(fn_mesh_msh), f"Mesh file '{fn_coil}' is missing."
if args_dict["neuronmodel"]:
    if layerid == 'L23':
        optim_dir = os.path.join(subject_dir,
                                 f"opt",
                                 f"target_{np.array2string(target, formatter={'float_kind': '{0:+0.2f}'.format})}"
                                 f"_neuron_L23",
                                 f"mesh_{mesh_idx}",
                                 f"{qoi}")
    elif layerid == 'L5':
        optim_dir = os.path.join(subject_dir,
                                 f"opt",
                                 f"target_{np.array2string(target, formatter={'float_kind': '{0:+0.2f}'.format})}"
                                 f"_neuron_L5",
                                 f"mesh_{mesh_idx}",
                                 f"{qoi}")

elif args_dict["cosine_model"]:
    optim_dir = os.path.join(subject_dir,
                             f"opt",
                             f"target_{np.array2string(target,  formatter={'float_kind': '{0:+0.2f}'.format})}"
                             f"_norm",
                             f"mesh_{mesh_idx}",
                             f"{qoi}")

else:
    optim_dir = os.path.join(subject_dir,
                             f"opt",
                             f"target_{np.array2string(target, formatter={'float_kind': '{0:+0.2f}'.format})}",
                             f"mesh_{mesh_idx}",
                             f"{qoi}")

if not os.path.exists(optim_dir):
    os.makedirs(optim_dir)
fn_e = os.path.join(optim_dir, f"e_scaled.hdf5")

print(f'\ttarget:           {target}')
print(f'\ttarget_radius:    {target_radius}')
print(f'\theadmesh:         {fn_mesh_msh}')
print(f'\toptim_dir:        {optim_dir}')

fn_conform_nii = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], subject.mesh[mesh_idx]["fn_mri_conform"])
try:
    fn_exp_nii = subject.exp[exp_idx]["fn_mri_nii"][0][0]
except KeyError:
    print(f"Experiment {exp_idx} not found.")
    fn_exp_nii = fn_conform_nii

# determine target region
# ========================================================================================================
roi_pynibs = pynibs.load_roi_surface_obj_from_hdf5(subject.mesh[mesh_idx]['fn_mesh_hdf5'])[roi_idx]

if args_dict["neuronmodel"]:
    for layer in roi_pynibs.layers:
        if layer.id == layerid:
            nodes = layer.surface.nodes.node_coord
            con = layer.surface.elm.node_number_list[:, :3] - np.min(layer.surface.elm.node_number_list[:, :3])
            break
    else:
        raise ValueError(f"Layer {layerid} was not found in roi {roi_idx}.")
    tri_center_layer = np.mean(nodes[con], axis=1)
    target_mask = np.linalg.norm(tri_center_layer - target, axis=1) < target_radius
    target_mask_idx = np.where(target_mask)[0]
else:
    target_mask = np.linalg.norm(roi_pynibs.tri_center_coord_mid - target, axis=1) < target_radius
    target_mask_idx = np.where(target_mask)[0]

if not target_mask.any():
    raise ValueError("TARGET: No ROI triangle found withing target_radius!")

# Initialize structure
# ========================================================================================================
tms_opt = opt_struct.TMSoptimize()
tms_opt.fnamehead = fn_mesh_msh
tms_opt.pathfem = optim_dir
tms_opt.fnamecoil = fn_coil
tms_opt.target = target
tms_opt.search_radius = opt_search_radius
tms_opt.search_angle = opt_search_angle
tms_opt.angle_resolution = opt_angle_resolution
tms_opt.spatial_resolution = opt_spatial_resolution
tms_opt.smooth = opt_smooth
tms_opt.distance = opt_distance
tms_opt.target_size = target_radius
mesh = mesh_io.read_msh(fn_mesh_msh)

# Run optimization
# ========================================================================================================
if not (os.path.exists(f'{optim_dir}{os.sep}opt_coil_pos.xml') and
        os.path.exists(f'{optim_dir}{os.sep}opt_coil_pos.hdf5')):
    # determine all possible coil positions
    tms_opt._prepare()
    pos_matrices = tms_opt._get_coil_positions()
    fn_coilpos_hdf5 = os.path.join(optim_dir, "search_positions.hdf5")
    pynibs.write_coil_pos_hdf5(fn_hdf=os.path.join(optim_dir, "search_positions.hdf5"),
                               centers=np.vstack([p[0:3, 3] for p in pos_matrices]),
                               m0=np.vstack([p[0:3, 0] for p in pos_matrices]),
                               m1=np.vstack([p[0:3, 1] for p in pos_matrices]),
                               m2=np.vstack([p[0:3, 2] for p in pos_matrices]),
                               datanames=None, data=None, overwrite=True)
    pos_matrices = np.array(pos_matrices)
    pos_matrices = np.swapaxes(pos_matrices, 0, 2)

    # run bruteforce e-field simulations
    cmd = f'{sys.executable} {os.path.join(scripts_folder, "run_simnibs_online.py")} '
    cmd += f" --folder '{optim_dir}' "
    cmd += f" --fn_subject '{fn_subject}' "
    cmd += f" --fn_coilpos '{fn_coilpos_hdf5}' "
    cmd += f"--anisotropy_type {anisotropy_type} "
    cmd += f"-m '{mesh_idx}' "
    cmd += f"--fn_coil '{fn_coil}' "
    cmd += f"-r {roi_idx} "
    cmd += f"--hdf5_fn '{fn_e}' "
    cmd += " --calc_theta_grad" if args_dict["neuronmodel"] else ""

    print(f"Running {cmd}")
    os.system(cmd)

    # load e-fields (and theta, gradient)
    with h5py.File(fn_e, "r") as f:
        if args_dict["neuronmodel"]:
            datanames = "E_eff"
            e_qoi = f[f"/{layerid}/{qoi}"][:, target_mask_idx]
            theta = f[f"/{layerid}/E_theta"][:, target_mask_idx]
            gradient = f[f"/{layerid}/E_gradient"][:, target_mask_idx]

            # determine effective e-field in case of neuron optimization
            e_matrix = calc_e_effective(e=e_qoi,
                                        layerid=layerid,
                                        theta=theta,
                                        gradient=gradient,
                                        neuronmodel=neuronmodel,
                                        mep=None,
                                        waveform=waveform)
        # use the normal component E if cosine_model selected
        elif args_dict["cosine_model"]:
            datanames = "abs_E_norm"
            e_norm = np.abs(f["E_norm"][:, target_mask_idx])
            e_matrix = e_norm
        else:
            datanames = "E_mag"
            e_qoi = f[qoi][:, target_mask_idx]
            e_matrix = e_qoi

    # determine best coil position
    idx_best_coil_pos = np.argmax(np.mean(e_matrix, axis=1))
    opt_matsim = pos_matrices[:, :, idx_best_coil_pos]
    print("Transposing matsimnibs -- why is this needed?")
    opt_matsim = np.transpose(opt_matsim)
    # save best coil position
    with h5py.File(os.path.join(optim_dir, 'opt_coil_pos.hdf5'), "w") as f:
        f.create_dataset(name="matsimnibs", data=opt_matsim)

    if len(opt_matsim.shape) == 2:
        opt_matsim = opt_matsim[:, :, np.newaxis]

    matlocalite = pynibs.nnav2simnibs(fn_exp_nii=fn_exp_nii,
                                      fn_conform_nii=fn_conform_nii,
                                      m_nnav=opt_matsim,
                                      target='nnav',
                                      fsl_cmd='FSL',
                                      nnav_system='localite')

    # write instrument marker file
    write_tms_navigator_im(matlocalite, os.path.join(optim_dir, 'opt_coil_pos.xml'), names='opt', overwrite=True)
    pynibs.create_stimsite_from_matsimnibs(os.path.join(optim_dir, 'opt_coil_pos.hdf5'), opt_matsim, overwrite=True)

else:
    print(f"Optimal coordinates already exist in:")
    print(f" > {os.path.join(optim_dir, 'opt_coil_pos.xml')}")
    print(f" > {os.path.join(optim_dir, 'opt_coil_pos.hdf5')}")
    print(f"Skipping optimization and reading optimal coordinates...")

    with h5py.File(os.path.join(optim_dir, 'opt_coil_pos.hdf5'), "r") as f:
        opt_matsim = f["matsimnibs"][:]

# save all coil positions together with electric field values in .xmdf file for plotting
fn_search_positions = os.path.join(optim_dir, "search_positions.hdf5")
fn_e = os.path.join(optim_dir, 'e_scaled.hdf5')

with h5py.File(fn_search_positions, "r") as f:
    centers = f["centers"][:]
    m0 = f["m0"][:]
    m1 = f["m1"][:]
    m2 = f["m2"][:]
shutil.copy(fn_search_positions, fn_search_positions.replace('.hdf5', f'_{time.time()}.hdf5'))

# get e-fields at target for each coil position
with h5py.File(fn_e, "r") as f:
    e_target = np.mean(e_matrix, axis=1)[:, np.newaxis]

pynibs.write_coil_pos_hdf5(fn_hdf=fn_search_positions,
                           centers=centers,
                           m0=m0,
                           m1=m1,
                           m2=m2,
                           datanames=datanames,
                           data=e_target,
                           overwrite=True)

# run final simulation at optimal coil position
# ========================================================================================================

# Setting up SimNIBS SESSION
# ========================================================================================================
# print("Setting up SimNIBS SESSION")
# S = simnibs.sim_struct.SESSION()
# S.fnamehead = fn_mesh_msh
# S.pathfem = os.path.join(optim_dir, "e_opt")
# S.fields = "vDeE"
# S.open_in_gmsh = False
# S.fname_tensor = None
# S.map_to_surf = True
# S.map_to_fsavg = False
# S.map_to_MNI = False
# S.subpath = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], f"m2m_{subject.id}")
#
# if not os.path.exists(S.pathfem):
#     os.makedirs(S.pathfem)
#
# # Define the TMS simulation and setting up conductivities
# # ========================================================================================================
# tms = [S.add_tmslist()]
# tms[0].fnamecoil = fn_coil
# tms[0].cond[0].value = 0.126  # WM
# tms[0].cond[1].value = 0.275  # GM
# tms[0].cond[2].value = 1.654  # CSF
# tms[0].cond[3].value = 0.01   # Skull
# tms[0].cond[4].value = 0.465  # Scalp
# tms[0].anisotropy_type = anisotropy_type
#
# if subject.mesh[mesh_idx]['fn_tensor_vn'] is not None:
#     tms[0].fn_tensor_nifti = os.path.join(subject.mesh[mesh_idx]["mesh_folder"],
#                                           subject.mesh[mesh_idx]['fn_tensor_vn'])
# else:
#     tms[0].fn_tensor_nifti = None
#
# tms[0].excentricity_scale = None
#
# # Define the optimal coil position
# # ========================================================================================================
# pos = tms[0].add_position()
# pos.matsimnibs = opt_matsim[:, :, 0]
# pos.distance = 0.1
# pos.didt = 1e6  # coil current rate of change in A/s (1e6 A/s = 1 A/us)
# pos.postprocess = S.fields
#
# # Running simulations
# # ========================================================================================================
# print("Running SimNIBS")
# filenames = simnibs.run_simnibs(S, cpus=1)
#
# print("Saving results in .hdf5 format.")
# if type(filenames) is not list:
#     filenames = [filenames]
#
# pynibs.simnibs_results_msh2hdf5(fn_msh=filenames,
#                                 fn_hdf5=[os.path.join(S.pathfem, "e")],
#                                 S=S,
#                                 pos_tms_idx=[0],    # indices of different "coils"
#                                 pos_local_idx=[0],  # indices of associated positions
#                                 subject=subject,
#                                 mesh_idx=mesh_idx,
#                                 overwrite=True,
#                                 mid2roi=True,
#                                 verbose=True)
print("=== DONE ===")
