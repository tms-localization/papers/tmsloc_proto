#!/usr/bin/env python

"""
This is a wrapper for simnibs.mni2subject_coords() to transform Talairach/MNI coordinates to subject space.
"""

import os
import pynibs
import pathlib
import argparse
import numpy as np

from simnibs import mni2subject_coords

parser = argparse.ArgumentParser(description='Transform MNI group coordinates to individual subject')
parser.add_argument('-s', '--fn_subject',
                    help='Filename (incl. path) of subject object file', required=True, type=str)
parser.add_argument('-m', '--mesh_idx',
                    help='Mesh idx', required=True)

args = parser.parse_args()

fn_subject = args.fn_subject
mesh_idx = args.mesh_idx
scripts_folder = pathlib.Path(__file__).parent.absolute()

subject = pynibs.load_subject(fn_subject)
m2m_folder = os.path.join(subject.mesh[mesh_idx]["mesh_folder"], f"m2m_{subject.id}")

# Coordinates taken from Maykaa et al., 2006, doi: 10.1016/j.neuroimage.2006.02.004
coords_tal = {
    'left-PMd': [-30, -4, 85],
    'left-PMv': [-50, 5, 22],
    'left-S1': [-40, -24, 50],
    'left-M1': [-37, -21, 58],
}

# transform into MNI
coords_mni = {key: pynibs.tal2mni(coord, direction='tal2mni', style='nonlinear').round(2) for
              key, coord in coords_tal.items()}

print(f"Transforming group level coordinates to subject space for {subject.id}:\n")
print(f"{'   Target': <11} {'Subject space': <15}{'|  '}{'  MNI space'}    |  {'Talairach space'}")
print("=" * 65)

for key in coords_mni.keys():
    print(f"{key: >10}: "
          f"{np.array2string(mni2subject_coords(coords_mni[key], m2m_folder), formatter={'float_kind': '{0:+3.0f}'.format})}  |  "
          f"{np.array2string(np.array(coords_mni[key]), formatter={'float_kind': '{0:+3.0f}'.format})}  |  "
          f"{np.array2string(np.array(coords_tal[key]), formatter={'float_kind': '{0:+3.0f}'.format})}")
print("-" * 65)
